
![Image of code coverage](doc_images/banner.png)


# Cabify Mobile Challenge

This repository contains an application simulating a shop for the Cabify code challenge.


<img src="doc_images/demo.gif" alt="demo" width="240"/>

## Getting Started

To run the project:

1. Clone this repository or download the code.
2. Open  `Cabifycc.xcodeproj`.
3. Click Run.


### Prerequisites


* Xcode 11.3.1
* iOS 13
  

## Application Features

  
#### Home Screen
* Display list of products.
* Products highlight with promotion.
* View items count and price preview.
* Pull to refresh.

#### Product Detail Screen
- Edit number of items.
- Add to basket.
- View extra information if promotion is applicable.

#### Basket View Screen
- List of selected products.
- Detailed information for order value, total and savings.
- Swipe left to quickly remove an item on the list.
- Pay simulation.


#### Dark Mode
<img src="doc_images/dark_mode.gif" alt="dark mode" width="240"/>

#### Dynamic Font Size
<img src="doc_images/dynamic_font_size.gif" alt="dynamic font size" width="240"/>

#### Other features
* Haptic feedback.
* Localization.
* Portrait and landscape orientation.





## Technology Considerations


### MVVM+C

I decided to use `MVVM+Coordinators` and protocols for their advantages in terms of testability and scalability. Coordinators allow to control the navigation flow easily and remove that responsability from the ViewControllers, making them agnostic of other components and improving their reusability. In other words, they improve the isolation of components.

### Combine

Combine is a reactive programming framework recently released by Apple. I have used this framework in the network layer, making the data binding simple and straightforward.

Also, I have used Combine to communicate the state of components like interactors to the view model. This makes it easy to notify when a call to the API is happening or whether it failed or succeeded.


### Architecture Diagram

Generally speaking, I have followed the approach you can see in the following diagram. A coordinator will start everything, it will create a view model that will provide representation to a view. The view model will be in charge of forwarding the interactions from the view to an interactor or to the coordinator, depending on the nature of the interaction (navigation or data related).

![Basic architecture](/doc_images/basic_architecture.png)

The app has a top-level  `root coordinator` that will control the navigation between the `list coordinator` and the `basket coordinator`. Then the two child coordinators will have their own view models and interactors to perform the representation of the view and the actions.

![Basic architecture](/doc_images/app_architecture.png)


### Testability

I have added a total of **169 meaningful unit tests** to all the classes components, reaching a **92.9% of code coverage**. I am aware that this number is just an aproximation to reality in Xcode, but I am confident that the most important features and interactions are throughly covered.

![Image of code coverage](/doc_images/code_coverage.png)



### Swift Package Manager

I have used Swift Package Manager to handle dependecies ([Reachability library](https://github.com/ashleymills/Reachability.swift)). I chose it over other dependency managers because it is native, quick to setup and avoids some of the problems you find with other third-party managers.


### Network Layer

#### Products Service:

The assignment suggested to bear in mind that the promotions could change depending on the season. This is why I decided to make them dynamic and not hardcoded. I hosted another static JSON representing the available promotions. In the future, when the backend is ready to return a list of promotions, it would be fairly easy to update the app to consume it.

In order to fetch products and promotions I have created a service that handles the two network calls. Then the interactor will zip together the results of both calls to update the model accordingly: it will check if there is any match with the `code` of the product and the `prooduct code` of the promotion. If they are the same then it will link them.
  
  ```javascript
  {
    "promotions": [
      {
        "productCode": "VOUCHER",
        "description": "Get 2 for 1 in Vouchers for Cabify.",
        "typeCode": "twoForOne"
      },
      {
        "productCode": "TSHIRT",
        "description": "Buy 3 or more and get them for a reduced price.",
        "typeCode": "reducedPrice",
        "minimumQuantity": 3,
        "reducedPrice": 19.0
      }
    ]
  }
  ```
Promotions JSON URL: https://api.myjson.com/bins/veb0i
  
#### Purchase Service:

Although the assignment didn't ask for a real payment system, the app has a purchase service that could potentially hit a payment endpoint in the backend. Currently it will display an alert saying the purchase was sucessfull as long as there is internet connection, otherwise it will alert about an error.

## Future Improvements & Additional Features

- [ ] Change quantity of items selected in the basket.
- [ ] Adjust layouts for iPad.
- [ ] Possibly connect more components with Combine.
- [ ] UI Tests.
- [ ] Bonus: Offline Mode.
