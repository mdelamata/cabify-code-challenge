//
//  ListInteracting.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import Combine

protocol ListInteracting {
    var products: CurrentValueSubject<[Product], Never> { get }
    var checkout: CurrentValueSubject<Checkout, Never> { get }
    var state: CurrentValueSubject<ListInteractorState, Never> { get }
    var basket: Basket { get }
    
    func updateBasket(basket: Basket)
    func setBasket(product: Product, withQuantity quantity: Int)
    func basketQuantity(for product: Product) -> Int
    
    func fetchItems()
}
