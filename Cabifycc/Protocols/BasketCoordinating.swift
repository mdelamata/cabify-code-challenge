//
//  BasketCoordinating.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

protocol BasketCoordinating: class, Alerting {
    func closeBasket(checkout: Checkout)
}
