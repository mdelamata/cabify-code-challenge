//
//  RootCoordinating.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

protocol RootCoordinating {
    func createListViewController()
    func createBasketViewController(checkout: Checkout)
    func updateCheckout(_ checkout: Checkout)
}
