//
//  ProductDetailViewModelProtocol.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

protocol ProductDetailViewModelProtocol {
    var title: String { get }
    var price: String { get }
    var descriptionItem: String { get }
    var currentQuantity: String { get }
    var chooseQuantity: String { get }
    var addToBasket: String { get }
    
    func increaseItemQuantity()
    func decreaseItemQuantity()
    func addItemsToBasket()
    func dismiss()
    
    var renderAction: ((ProductDetailState) -> ())? { get set }
}

