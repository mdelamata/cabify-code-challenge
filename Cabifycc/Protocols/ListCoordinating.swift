//
//  ListCoordinating.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

protocol ListCoordinating: class, Alerting {
    func showBasketView(checkout: Checkout)
    func showDetail(product: Product, currentQuantity: Int)
    func dismissDetail(product: Product, newQuantity: Int?)
    func updateCheckout(_ checkout: Checkout)
}
