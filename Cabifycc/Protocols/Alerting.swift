//
//  Alerting.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 26/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

protocol Alerting {
    func showAlert(viewModel: AlertViewModel)
}

extension Alerting where Self: Coordinating {
    
    func showAlert(viewModel: AlertViewModel) {
        let alertController = AlertControllerFactory.createAlertController(viewModel: viewModel)
        
        navigationController.visibleViewController?.present(alertController, animated: true, completion: nil)
    }
}
