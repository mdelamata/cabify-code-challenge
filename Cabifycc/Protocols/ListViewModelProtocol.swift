//
//  ListViewModelProtocol.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

protocol ListViewModelProtocol {
    func viewBasketTapped()
    func updateBasket(basket: Basket)
    func setBasket(product: Product, withQuantity quantity: Int)
    func basketQuantity(for product: Product) -> Int
    func configure(tableView: UITableView)
    func fetchItems()
    func shouldShowViewBasket() -> Bool
    func productsCount() -> String
    func totalPrice() -> String
    func title() -> String
    func viewBasketTitle() -> String

    var renderAction: ((ListViewState) -> ())? { get set }
}
