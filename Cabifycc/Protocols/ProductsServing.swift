//
//  ProductsServing.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import Combine

protocol ProductsServing {
    func fetchProducts() -> AnyPublisher<ProductList, Error>
    func fetchPromotions() -> AnyPublisher<PromotionList, Error>
}
