//
//  CoordinatorFactoryProtocol.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

protocol CoordinatorFactoryProtocol {
    func createListCoordinator(navigationController: UINavigationController) -> Coordinating
    func createBasketCoordinator(navigationController: UINavigationController, with checkout: Checkout) -> Coordinating
}
