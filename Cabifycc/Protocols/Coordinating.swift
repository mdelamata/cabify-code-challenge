//
//  Coordinating.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

protocol Coordinating: AnyObject {
    var parentCoordinator: Coordinating? { get set }
    var childCoordinators: [Coordinating] { get set }
    var navigationController: UINavigationController { get set }
    
    func start()
    func childDidFinish(_ child: Coordinating)
}
