//
//  BasketViewModelProtocol.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

protocol BasketViewModelProtocol {
    func title() -> String
    func orderValue() -> String
    func orderValueAmount() -> String
    func savings() -> String
    func savingsAmount() -> String
    func total() -> String
    func totalAmount() -> String
    func payButtonTitle() -> String
    func payButtonEnabled() -> Bool
    func purchaseAlertTitle() -> String
    func purchaseAlertDismiss() -> String
    func payTapped()
    func closeView()
    
    func configure(tableView: UITableView)
    
    var renderAction: ((BasketViewState) -> ())? { get set }
}
