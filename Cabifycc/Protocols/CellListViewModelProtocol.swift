//
//  CellListViewModelProtocol.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

protocol CellListViewModelProtocol {
    func getCellViewModel(at indexPath: IndexPath) -> ProductCellViewModel
    func numberOfItems() -> Int
    func didSelectRow(at indexPath: IndexPath)
}
