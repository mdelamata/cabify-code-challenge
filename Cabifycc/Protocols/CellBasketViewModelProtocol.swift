//
//  CellBasketViewModelProtocol.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

protocol CellBasketViewModelProtocol {
    func getCellViewModel(at indexPath: IndexPath) -> CheckoutItemCellViewModel
    func numberOfItems() -> Int
    func didSelectRow(at indexPath: IndexPath)
    func didDeleteRow(at indexPath: IndexPath)
}
