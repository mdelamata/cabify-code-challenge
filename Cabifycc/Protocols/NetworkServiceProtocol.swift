//
//  NetworkServiceProtocol.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import Foundation
import Combine

protocol NetworkServiceProtocol: class {
    @discardableResult
    func load<T: Decodable>(_ resource: Resource<T>) -> AnyPublisher<T, Error>
}

enum NetworkError: Error {
    case noInternetError
    case requestError
    case jsonDecodingError
}
