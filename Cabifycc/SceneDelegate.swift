//
//  SceneDelegate.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

      var window: UIWindow?
      
      var coordinator: RootCoordinator!
      
      func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {

          guard let windowScene = (scene as? UIWindowScene) else { return }
          
          let navController = UINavigationController()
          
          coordinator = RootCoordinator(navigationController: navController)
          
          coordinator?.start()
          
          // create a basic UIWindow and activate it
          window = UIWindow(frame: windowScene.coordinateSpace.bounds)
          window?.windowScene = windowScene
          window?.rootViewController = navController
          window?.makeKeyAndVisible()
      }
}

