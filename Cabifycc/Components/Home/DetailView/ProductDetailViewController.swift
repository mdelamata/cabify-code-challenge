//
//  ProductDetailViewController.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

final class ProductDetailViewController: UIViewController {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var chooseAmountLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var decreaseButton: UIButton!
    @IBOutlet weak var increaseButton: UIButton!
    @IBOutlet weak var addToBasketButton: UIButton!
   
    var feedbackGenerator: FeedbackGenerating! = FeedbackGenerator()
    
    var viewModel: ProductDetailViewModelProtocol!
    
    deinit {
        print("deinit \(String(describing: type(of: self)))")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        presentationController?.delegate = viewModel as? UIAdaptivePresentationControllerDelegate
        
        configureUI()
        
        viewModel.renderAction = { [weak self] state in
            
            guard let self = self else { return }
            
            switch state {
            case .updateQuantity:
                
                UIView.animate(withDuration: 0.1,
                               delay: 0,
                               usingSpringWithDamping: 0.5,
                               initialSpringVelocity: 0.5,
                               options: .curveEaseOut,
                               animations: {
                        self.amountLabel.text = self.viewModel.currentQuantity
                        self.amountLabel.transform = .init(scaleX: 1.1, y: 1.1)
                }, completion: {  finished in
                    self.amountLabel.transform = .identity
                })
            }
        }
    }
    
    func configureUI() {
        titleLabel.text = viewModel.title
        descriptionLabel.text = viewModel.descriptionItem
        priceLabel.text = viewModel.price
        amountLabel.text = viewModel.currentQuantity
        chooseAmountLabel.text = viewModel.chooseQuantity
        addToBasketButton.setTitle(viewModel.addToBasket, for: .normal)
    }
    
    //MARK: - IBAction Methods
    
    @IBAction func decreaseButtonPressed(_ sender: UIButton) {
        viewModel.decreaseItemQuantity()
        feedbackGenerator.selectionDidChange()
    }
    
    @IBAction func increaseButtonPressed(_ sender: UIButton) {
        viewModel.increaseItemQuantity()
        feedbackGenerator.selectionDidChange()
    }
    
    @IBAction func addToBaketButtonPressed(_ sender: UIButton) {
        viewModel.addItemsToBasket()
        feedbackGenerator.rigidImpactOccurred()
    }
    
    @IBAction func closeButtonPressed(_ sender: UIButton) {
        viewModel.dismiss()
    }
}
