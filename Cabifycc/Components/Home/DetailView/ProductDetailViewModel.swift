//
//  ItemDetailViewModel.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit
import Combine

enum ProductDetailState {
    case updateQuantity
}

final class ProductDetailViewModel: NSObject, ProductDetailViewModelProtocol {
    
    var coordinator: ListCoordinating
    var product: Product
    var quantity: CurrentValueSubject<Int, Never>
    
    var subscriber: AnyCancellable!
    
    init(coordinator: ListCoordinating, product: Product, quantity: Int) {
        
        self.coordinator = coordinator
        self.product = product
        self.quantity = CurrentValueSubject<Int, Never>(quantity)
        
        super.init()
        
        subscribe()
    }
    
    deinit {
        print("deinit \(String(describing: type(of: self)))")
    }
    
    var renderAction: ((ProductDetailState) -> ())?
    
    func subscribe() {
        subscriber = quantity.map({ quantity -> ProductDetailState in
            return .updateQuantity
        }).sink(receiveValue: { [weak self] state in
            self?.renderAction?(state)
        })
    }
    
    
    //MARK:- Data
    
    var title: String {
        product.name
    }
    
    var price: String {
        product.price.formattedString
    }
    
    var descriptionItem: String {
        product.promotion?.description ?? ""
    }
    
    var currentQuantity: String {
        String(quantity.value)
    }
    
    var chooseQuantity: String {
        "product_detail_view.choose_quantity".localized
    }
    
    var addToBasket: String {
        "product_detail_view.add_to_basket".localized
    }
    
    //MARK: - Action Methods
    
    func increaseItemQuantity() {
        quantity.send(quantity.value + 1)
    }
    
    func decreaseItemQuantity() {
        if quantity.value == 0 {
            return
        }
        quantity.send(quantity.value - 1)
    }
    
    func addItemsToBasket(){
        closeView(withQuantity: quantity.value)
    }
    
    func dismiss() {
        closeView(withQuantity: nil)
    }
    
    func closeView(withQuantity quantity: Int?) {
        coordinator.dismissDetail(product: product, newQuantity: quantity)
    }
}

extension ProductDetailViewModel: UIAdaptivePresentationControllerDelegate {
    
    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        dismiss()
    }
}
