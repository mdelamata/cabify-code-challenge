//
//  ListViewController.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

final class ListViewController: UIViewController {
    
    @IBOutlet weak var activityIndicatorView: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var viewBasketContainerView: UIView!
    @IBOutlet weak var productsCountLabel: UILabel!
    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var viewBasketButton: UIButton!
    @IBOutlet weak var viewBasketContainerViewbottomLayoutConstraint: NSLayoutConstraint!
    
    var refreshControl: UIRefreshControl!
    
    var viewModel: ListViewModelProtocol!
    
    deinit {
        print("deinit \(String(describing: type(of: self)))")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        updateData()
    }
    
    func configureUI(){
        
        hideTableView()
        
        title = viewModel.title()
        viewBasketButton.setTitle(viewModel.viewBasketTitle(), for: .normal)
        
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action:
            #selector(ListViewController.handleRefresh(_:)), for: .valueChanged)
        tableView.refreshControl = refreshControl
        
        viewModel.configure(tableView: tableView)
        
        hideViewBasket(animated: false)
        
        viewModel.renderAction = { [weak self] state in
            
            guard let self = self else { return }
            
            switch state {
            case .loading:
                self.showActivityIndicator()
                
            case .failure:
                self.showTableView()
                self.hideActivityIndicator()

            case .update:
                self.showTableView()
                self.tableView.reloadData()
                self.updateViewBasketView()
                self.hideActivityIndicator()

            default:
                break
            }
        }
    }
    
    func updateData() {
        viewModel.fetchItems()
    }
    
    @objc func handleRefresh(_ refreshControl: UIRefreshControl) {
        updateData()
    }
    
    func updateViewBasketView() {
        
        if viewModel.shouldShowViewBasket() {
            
            animateViewBasketIfNeeded()
            
            productsCountLabel.text = viewModel.productsCount()
            totalPriceLabel.text = viewModel.totalPrice()
            
        } else {
            hideViewBasket(animated: true)
        }
    }
    
    func hideTableView() {
        tableView.alpha = 0
    }
    
    func showTableView() {
        UIView.animate(withDuration: 0.3) {
            self.tableView.alpha = 1
        }
    }
    
    func animateViewBasketIfNeeded() {
        if viewBasketContainerViewbottomLayoutConstraint.constant != 0 {
            
            viewBasketContainerViewbottomLayoutConstraint.constant = 0
            
            UIView.animate(withDuration: 0.3, animations: {
                self.viewBasketContainerView.alpha = 1
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    func hideViewBasket(animated: Bool) {
        
        viewBasketContainerViewbottomLayoutConstraint.constant = viewBasketContainerView.frame.height + (view.window?.safeAreaInsets.bottom ?? 0)
        
        UIView.animate(withDuration: animated ? 0.3: 0, animations: {
            self.viewBasketContainerView.alpha = 0
        }, completion: nil)
    }
    
    func showActivityIndicator() {
        activityIndicatorView.isHidden = refreshControl.isRefreshing
    }
    
    func hideActivityIndicator() {
        activityIndicatorView.isHidden = true
        refreshControl.endRefreshing()
    }

    //MARK:- IBAction Methods
    
    @IBAction func viewBasketButtonPressed(_ sender: UIButton) {
        viewModel.viewBasketTapped()
    }
}
