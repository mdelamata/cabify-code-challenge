//
//  ListViewModel.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit
import Combine

enum ListViewState {
    case loading
    case failure
    case update
    case idle
}

final class ListViewModel {
    
    weak var coordinator: ListCoordinating?
    var interactor: ListInteracting
    
    var listTableViewDataSource: ListTableViewDataSource!
    var listTableViewDelegate: ListTableViewDelegate!
    
    lazy var factory: ProductCellViewModelFactoryProtocol = ProductCellViewModelFactory()
    
    var renderAction: ((ListViewState) -> ())?
    var interactorStateSubscriber: AnyCancellable!
    
    init(coordinator: ListCoordinating, interactor: ListInteracting) {
        self.coordinator = coordinator
        self.interactor = interactor
        
        subscribe()
    }
    
    deinit {
        print("deinit \(String(describing: type(of: self)))")
    }
    
    func subscribe() {
                
        interactorStateSubscriber = self.interactor.state.map( { [weak self] result -> ListViewState in
            
            switch result {
            case .loading: return .loading
            case .failure(let error):
                self?.coordinator?.showAlert(viewModel: error.toAlertViewModel)
                return .failure
            case .success: return .update
            case .idle: return .idle
            }
            
        }).eraseToAnyPublisher().sink(receiveValue: { listViewState in
            self.updateView(state: listViewState)
        })
    }
    
    func updateView(state: ListViewState) {
        renderAction?(state)
    }
}
    
// MARK: - ListViewModelProtocol methods

extension ListViewModel: ListViewModelProtocol {

    // MARK: - TableView
    
    func configure(tableView: UITableView) {
        
        listTableViewDataSource = ListTableViewDataSource(viewModel: self)
        listTableViewDelegate = ListTableViewDelegate(viewModel: self)
        
        tableView.dataSource = listTableViewDataSource
        tableView.delegate = listTableViewDelegate
    }
    
    // MARK: - Fetch
    
    func fetchItems() {
        interactor.fetchItems()
    }
    
    // MARK: - Basket
    
    func viewBasketTapped() {
        coordinator?.showBasketView(checkout: interactor.checkout.value)
    }
    
    func updateBasket(basket: Basket) {
        interactor.updateBasket(basket: basket)
        updateView(state: .update)
    }
    
    func setBasket(product: Product, withQuantity quantity: Int) {
        interactor.setBasket(product: product, withQuantity: quantity)
        updateView(state: .update)
    }
    
    func basketQuantity(for product: Product) -> Int {
        return interactor.basketQuantity(for: product)
    }
    
    func shouldShowViewBasket() -> Bool {
        interactor.checkout.value.productsQuantity > 0
    }
    
    func productsCount() -> String {
        String(interactor.checkout.value.productsQuantity)
    }
    
    func totalPrice() -> String {
        interactor.checkout.value.total.formattedString
    }
    
    //MARK:- Strings
    
    func title() -> String {
        "list_view.title".localized
    }
    
    func viewBasketTitle() -> String {
        "list_view.view_basket".localized
    }
}


//MARK:- ListViewModelCellViewModelProtocol Methods

extension ListViewModel: CellListViewModelProtocol {
    
    func getCellViewModel(at indexPath: IndexPath) -> ProductCellViewModel {
        let product = interactor.products.value[indexPath.row]
        return factory.createProductCellViewModel(for: product, amount: basketQuantity(for: product))
    }
    
    func numberOfItems() -> Int {
        interactor.products.value.count
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        let product = interactor.products.value[indexPath.row]
        let quantity = basketQuantity(for: product)
        coordinator?.showDetail(product: product, currentQuantity: quantity == 0 ? 1 : quantity)
    }
}
