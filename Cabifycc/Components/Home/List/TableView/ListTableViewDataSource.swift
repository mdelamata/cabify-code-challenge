//
//  ListTableViewDataSource.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

final class ListTableViewDataSource: NSObject {
    
    var viewModel: CellListViewModelProtocol!
    
    init(viewModel: CellListViewModelProtocol) {
        self.viewModel = viewModel
    }
}

extension ListTableViewDataSource: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItems()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ProductTableViewCell.reusableIdentifier,
                                                 for: indexPath) as! ProductTableViewCell
        let cellViewModel = viewModel.getCellViewModel(at: indexPath)
        cell.configure(viewModel: cellViewModel)
        return cell
    }

}
