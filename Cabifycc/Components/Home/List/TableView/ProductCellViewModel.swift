//
//  ProductCellViewModel.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

struct ProductCellViewModel {
    
    let nameText: String
    let priceText: String
    let amountText: String
    let image: UIImage
    let hasPromotion: Bool
}
