//
//  ListTableViewDelegate.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

final class ListTableViewDelegate: NSObject {
    
    var viewModel: CellListViewModelProtocol
    
    init(viewModel: CellListViewModelProtocol) {
        self.viewModel = viewModel
    }
}

extension ListTableViewDelegate: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelectRow(at: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
