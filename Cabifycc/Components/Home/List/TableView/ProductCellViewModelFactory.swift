//
//  ProductCellViewModelFactory.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

struct ProductCellViewModelFactory: ProductCellViewModelFactoryProtocol {
    
    func createProductCellViewModel(for product: Product, amount: Int) -> ProductCellViewModel {
        
        ProductCellViewModel(nameText: product.name,
                             priceText: product.price.formattedString,
                             amountText: formattedAmount(amount: amount),
                             image: image(for: product.code),
                             hasPromotion: product.promotion != nil)
    }
    
    private func formattedAmount(amount: Int) -> String {
        
        return amount > 0 ? "x\(String(amount))" : ""
    }
    
    private func image(for code: String) -> UIImage {
        
        guard let image = UIImage(named: code.lowercased()) else {
            return UIImage(named: "unknown")!
        }
        return image
    }
}
