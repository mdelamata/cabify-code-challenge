//
//  ProductTableViewCell.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

class ProductTableViewCell: UITableViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var promotionBadgeImageView: UIImageView!

    static var reusableIdentifier: String {
        "productCell"
    }
    
    func configure(viewModel: ProductCellViewModel) {
        
        nameLabel.text = viewModel.nameText
        priceLabel.text = viewModel.priceText
        amountLabel.text = viewModel.amountText
        productImageView.image = viewModel.image
        
        amountLabel.isHidden = viewModel.amountText == ""
        promotionBadgeImageView.isHidden = viewModel.hasPromotion == false
    }
    
}
