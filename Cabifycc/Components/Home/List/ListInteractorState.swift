//
//  ListInteractorState.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

enum ListInteractorState {
    case idle
    case loading
    case success
    case failure(Error)
}

extension ListInteractorState: Equatable {
    static func == (lhs: ListInteractorState, rhs: ListInteractorState) -> Bool {
        switch (lhs, rhs) {
        case (.idle, .idle): return true
        case (.loading, .loading): return true
        case (.failure(let lhsError), .failure(let rhsError)): return areEqual(lhsError, rhsError)
        case (.success, .success): return true
        default: return false
        }
    }
}
