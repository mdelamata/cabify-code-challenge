//
//  ListCoordinator.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

final class ListCoordinator: Coordinating {
    
    weak var parentCoordinator: Coordinating?
    var childCoordinators = [Coordinating]()
    var navigationController: UINavigationController
    
    var listViewController: ListViewController!
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    deinit {
        print("deinit \(String(describing: type(of: self)))")
    }
    
    func start() {
        
        let listViewModel = ListViewModel(coordinator: self,
                                          interactor: ListInteractor())
        
        listViewController = UIStoryboard.main.instantiateViewController(identifier: "ListViewController")
        listViewController.viewModel = listViewModel
        navigationController.pushViewController(listViewController, animated: false)
    }
    
    func childDidFinish(_ child: Coordinating) {}
    
    func finish() {
        parentCoordinator?.childDidFinish(self)
    }
}

//MARK:- ListCoordinating Methods

extension ListCoordinator: ListCoordinating {
    
    func showBasketView(checkout: Checkout) {
        (parentCoordinator as? RootCoordinating)?.createBasketViewController(checkout: checkout)
    }
    
    func showDetail(product: Product, currentQuantity: Int) {
        
        let productDetailViewModel = ProductDetailViewModel(coordinator: self,
                                                            product: product,
                                                            quantity: currentQuantity)
        
        let productDetailViewController = UIStoryboard.main.instantiateViewController(identifier: "ProductDetailViewController") as! ProductDetailViewController
        productDetailViewController.viewModel = productDetailViewModel
        
        navigationController.present(productDetailViewController, animated: true, completion: nil)
    }
    
    func dismissDetail(product: Product, newQuantity: Int?) {
        
        if navigationController.presentedViewController != nil {
            navigationController.dismiss(animated: true, completion: { [weak self] in
                
                guard let listViewController = self?.listViewController else { return }
                
                if let newQuantity = newQuantity {
                    listViewController.viewModel.setBasket(product: product, withQuantity: newQuantity)
                }
            })
        }
    }
    
    func updateCheckout(_ checkout: Checkout) {
        
        listViewController.viewModel.updateBasket(basket: checkout.toBasket)
    }
}
