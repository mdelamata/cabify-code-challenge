//
//  ListInteractor.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import Foundation
import Combine


final class ListInteractor: ListInteracting {
    
    //Models
    let products = CurrentValueSubject<[Product], Never>([])
    let checkout = CurrentValueSubject<Checkout, Never>(Checkout.empty())
    var basket = Basket()
    
    //Interactor state
    let state = CurrentValueSubject<ListInteractorState, Never>(.idle)
    
    //External Components
    var productsService: ProductsServing = ProductsService()
    
    deinit {
        print("deinit \(String(describing: type(of: self)))")
    }
    
    //MARK: - Service Method
    
    func fetchItems() {
        
        self.state.send(.loading)
        
        _ = Publishers.Zip(productsService.fetchProducts(),
                           productsService.fetchPromotions())
            .eraseToAnyPublisher()
            .sink(receiveCompletion: { completion in
                
                switch completion {
                case .failure(let error):
                    self.state.send(.failure(error))
                    self.state.send(.idle)
                case .finished:
                    self.state.send(.success)
                    self.state.send(.idle)
                }
                
            }, receiveValue: { productsList, promotionList in
                
                let updatedProducts = self.assign(promotions: promotionList.promotions,
                                                  to: productsList.products)
                self.products.send(updatedProducts)
            })
    }
    
    //MARK: - Basket
    
    func updateBasket(basket: Basket) {
        self.basket = basket
        updateCheckout(basket: basket)
    }
    
    func setBasket(product: Product, withQuantity quantity: Int) {
        basket[product] = quantity == 0 ? nil : quantity
        updateCheckout(basket: basket)
    }
    
    func basketQuantity(for product: Product) -> Int {
        guard let quantity = basket[product] else {
            return 0
        }
        return quantity
    }
    
    func updateCheckout(basket: Basket) {
        checkout.send(basket.toCheckout)
    }
    
    
    //MARK: - Helper
    
    func assign(promotions: [Promotion], to products: [Product]) -> [Product] {
        
        let updatedProduct = products.map { product -> Product in
            
            if let promotion = promotions.filter({ $0.productCode == product.code }).first {
                var updatedProduct = product
                updatedProduct.updatePromotion(promotion: promotion)
                return updatedProduct
            }
            return product
        }
        return updatedProduct
    }
}
