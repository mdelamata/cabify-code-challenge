//
//  BasketCoordinator.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

final class BasketCoordinator: NSObject, Coordinating {

    weak var parentCoordinator: Coordinating?
    var childCoordinators = [Coordinating]()
    var navigationController: UINavigationController
    
    var checkout: Checkout
    
    init(navigationController: UINavigationController, checkout: Checkout) {
        self.navigationController = navigationController
        self.checkout = checkout
    }
    
    deinit {
        print("deinit \(String(describing: type(of: self)))")
    }
    
    func start() {
        
        let basketViewModel = BasketViewModel(coordinator: self,
                                              interactor: BasketInteractor(),
                                              checkout: checkout)
        
        guard
            let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "BasketViewControllerNavigationController") as? UINavigationController,
            let basketViewController = navController.viewControllers.first as? BasketViewController
        else {
            return
        }
                
        basketViewController.viewModel = basketViewModel
        
        navigationController.present(navController, animated: true, completion: nil)
    }
    
    func childDidFinish(_ child: Coordinating) {}
    
    func finish() {
        parentCoordinator?.childDidFinish(self)
    }
}

//MARK:- BasketCoordinating Methods

extension BasketCoordinator: BasketCoordinating {
    
    func closeBasket(checkout: Checkout) {
        
        if navigationController.presentedViewController != nil {
            navigationController.dismiss(animated: true, completion: nil)
        }
        
        (parentCoordinator as? RootCoordinating)?.updateCheckout(checkout)
        finish()
    }
}
