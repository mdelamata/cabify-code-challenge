//
//  BasketInteractor.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import Foundation
import Combine

struct BasketInteractor: BasketInteracting {

    var purchaseService: PurchaseServing = PurchaseService()
    
    func purchase(checkout: Checkout) -> AnyPublisher<Bool, Error>  {
        return purchaseService.pay(checkout: checkout)
    }
}
