//
//  CheckoutItemCellViewModel.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 26/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

struct CheckoutItemCellViewModel {
    
    let nameText: String
    let beforePromotionPriceText: String
    let afterPromotionPriceText: String
    let quantityText: String
    let image: UIImage
}
