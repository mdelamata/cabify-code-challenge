//
//  BasketTableViewDataSource.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 26/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

final class BasketTableViewDataSource: NSObject {
    
    var viewModel: CellBasketViewModelProtocol!
    
    init(viewModel: CellBasketViewModelProtocol) {
        self.viewModel = viewModel
    }
}

extension BasketTableViewDataSource: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItems()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CheckoutItemTableViewCell.reusableIdentifier,
                                                 for: indexPath) as! CheckoutItemTableViewCell
        let cellViewModel = viewModel.getCellViewModel(at: indexPath)
        cell.configure(viewModel: cellViewModel)
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            viewModel.didDeleteRow(at: indexPath)
        }
    }
}
