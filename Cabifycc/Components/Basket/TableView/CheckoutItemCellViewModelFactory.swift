//
//  CheckoutItemCellViewModelFactory.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 26/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

struct CheckoutItemCellViewModelFactory: CheckoutItemCellViewModelFactoryProtocol {
    
    func createCheckoutItemCellViewModel(for checkoutItem: CheckoutItem) -> CheckoutItemCellViewModel {
        
        var beforePromotionPrice = checkoutItem.beforePromotionPrice.formattedString
        let afterPromotionPrice = checkoutItem.afterPromotionPrice.formattedString
        
        if beforePromotionPrice == afterPromotionPrice {
            beforePromotionPrice = ""
        }
        
        return CheckoutItemCellViewModel(nameText: formattedName(name: checkoutItem.product.name, quantity: checkoutItem.quantity),
                                         beforePromotionPriceText: beforePromotionPrice,
                                         afterPromotionPriceText: afterPromotionPrice,
                                         quantityText: String(checkoutItem.quantity),
                                         image: image(for: checkoutItem.product.code))
    }
    
    private func formattedName(name: String, quantity: Int) -> String {
        "x\(quantity) \(name)"
    }
    
    private func image(for code: String) -> UIImage {
        
        guard let image = UIImage(named: code.lowercased()) else {
            return UIImage(named: "unknown")!
        }
        return image
    }
}
