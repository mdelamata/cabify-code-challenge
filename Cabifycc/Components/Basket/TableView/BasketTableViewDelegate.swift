//
//  BasketTableViewDelegate.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 26/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

final class BasketTableViewDelegate: NSObject {
    
    var viewModel: CellBasketViewModelProtocol
    
    init(viewModel: CellBasketViewModelProtocol) {
        self.viewModel = viewModel
    }
}

extension BasketTableViewDelegate: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.didSelectRow(at: indexPath)
    }
}
