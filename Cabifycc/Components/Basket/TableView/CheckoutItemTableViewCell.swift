//
//  CheckoutItemTableViewCell.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 26/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

class CheckoutItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var quantityLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var beforePromotionPriceLabel: UILabel!
    @IBOutlet weak var afterPromotionPriceLabel: UILabel!

    static var reusableIdentifier: String {
        "checkoutItemCell"
    }
    
    func configure(viewModel: CheckoutItemCellViewModel) {
        
        nameLabel.text = viewModel.nameText
        beforePromotionPriceLabel.attributedText = viewModel.beforePromotionPriceText.strikeThroughed
        afterPromotionPriceLabel.text = viewModel.afterPromotionPriceText
        quantityLabel.text = viewModel.quantityText
        productImageView.image = viewModel.image
        
        beforePromotionPriceLabel.isHidden = viewModel.beforePromotionPriceText == ""
    }
    
}
