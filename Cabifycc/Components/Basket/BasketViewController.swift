//
//  BasketViewController.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

final class BasketViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var orderValueLabel: UILabel!
    @IBOutlet weak var orderValueAmountLabel: UILabel!
    @IBOutlet weak var savingsLabel: UILabel!
    @IBOutlet weak var savingsAmountLabel: UILabel!
    @IBOutlet weak var totalLabel: UILabel!
    @IBOutlet weak var totalAmountLabel: UILabel!
    @IBOutlet weak var payButton: UIButton!
    
    var viewModel: BasketViewModelProtocol!
    
    deinit {
        print("deinit \(String(describing: type(of: self)))")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureUI()
        updateUI()
    }
    
    func configureUI() {
        
        title = viewModel.title()

        viewModel.configure(tableView: tableView)
        
        navigationController?.presentationController?.delegate = viewModel as? UIAdaptivePresentationControllerDelegate
        
        viewModel.renderAction = { [weak self] state in
            switch state {
            case .update:
                self?.updateUI()
            }
        }
    }
    
    func updateUI() {
                
        orderValueLabel.text = viewModel.orderValue()
        orderValueAmountLabel.text = viewModel.orderValueAmount()
        savingsLabel.text = viewModel.savings()
        savingsAmountLabel.text = viewModel.savingsAmount()
        totalLabel.text = viewModel.total()
        totalAmountLabel.text = viewModel.totalAmount()
        
        payButton.setTitle(viewModel.payButtonTitle(), for: .normal)
        payButton.isEnabled = viewModel.payButtonEnabled()
        payButton.alpha = viewModel.payButtonEnabled() ? 1 : 0.3

        tableView.reloadData()
    }
    
    
    //MARK: - IBAction Methods
    
    @IBAction func cancelButtonPressed(_ sender: UIBarButtonItem) {
        viewModel.closeView()
    }
    
    @IBAction func payButtonPressed(_ sender: UIButton) {
        viewModel.payTapped()
    }
}
