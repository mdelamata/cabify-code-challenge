//
//  BasketViewModel.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit
import Combine

enum BasketViewState {
    case update
}

final class BasketViewModel: NSObject {
    
    weak var coordinator: BasketCoordinating?
    var interactor: BasketInteracting
    
    var basketTableViewDataSource: BasketTableViewDataSource!
    var basketTableViewDelegate: BasketTableViewDelegate!
    
    lazy var factory: CheckoutItemCellViewModelFactoryProtocol = CheckoutItemCellViewModelFactory()
    
    var renderAction: ((BasketViewState) -> ())?
    
    var feedbackGenerator: FeedbackGenerating! = FeedbackGenerator()

    var checkout: Checkout
    
    init(coordinator: BasketCoordinating, interactor: BasketInteracting, checkout: Checkout) {
        self.coordinator = coordinator
        self.interactor = interactor
        self.checkout = checkout        
    }
    
    func updateView(state: BasketViewState) {
        renderAction?(state)
    }
    
    func dismissView() {
        coordinator?.closeBasket(checkout: checkout)
    }
    
    func purchaseDone() {
        
        let dismissAction: ()->() = { [weak self] in
            self?.checkout = Checkout.empty()
            self?.dismissView()
        }
        
        let alertViewModel = AlertViewModel(title: purchaseAlertTitle(),
                                            actions: [[purchaseAlertDismiss(): dismissAction]])
        
        coordinator?.showAlert(viewModel: alertViewModel)
        
        feedbackGenerator.heavyImpactOccurred()
    }
}

// MARK: - ListViewModelProtocol Methods

extension BasketViewModel: BasketViewModelProtocol {
    
    // MARK: - TableView
    
    func configure(tableView: UITableView) {
        
        basketTableViewDataSource = BasketTableViewDataSource(viewModel: self)
        basketTableViewDelegate = BasketTableViewDelegate(viewModel: self)
        
        tableView.dataSource = basketTableViewDataSource
        tableView.delegate = basketTableViewDelegate
    }
    
    // MARK: - Navigation

    func closeView() {
        dismissView()
    }
    
    // MARK: - Network call
    
    func payTapped() {
        _ = interactor.purchase(checkout: checkout)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .finished: break
                case .failure(let error):
                    self.coordinator?.showAlert(viewModel: error.toAlertViewModel)
                }
            }, receiveValue: { _ in
                self.purchaseDone()
            })
    }
    
    // MARK: - UI Data
    
    func title() -> String {
        "basket_view.tite".localized
    }
    
    func orderValue() -> String {
        "basket_view.order_value".localized
    }
    
    func orderValueAmount() -> String {
        (checkout.total + checkout.savings).formattedString
    }
    
    func savings() -> String {
        "basket_view.savings".localized
    }
    
    func savingsAmount() -> String {
        checkout.savings.formattedString
    }
    
    func total() -> String {
        "basket_view.total".localized
    }
    
    func totalAmount() -> String {
        checkout.total.formattedString
    }
    
    func payButtonTitle() -> String {
        "basket_view.pay".localized
    }
    
    func payButtonEnabled() -> Bool {
        checkout.productsQuantity > 0
    }
    
    func purchaseAlertTitle() -> String {
        "basket_view.purchase.alert.title".localized
    }
    
    func purchaseAlertDismiss() -> String {
        "basket_view.purchase.alert.dismiss".localized
    }
}

//MARK:- CellBasketViewModelProtocol Methods

extension BasketViewModel: CellBasketViewModelProtocol {
    
    func getCellViewModel(at indexPath: IndexPath) -> CheckoutItemCellViewModel {
        let checkoutItem = checkout.items[indexPath.row]
        return factory.createCheckoutItemCellViewModel(for: checkoutItem)
    }
    
    func numberOfItems() -> Int {
        checkout.items.count
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        
    }
    
    func didDeleteRow(at indexPath: IndexPath) {
        checkout.items.remove(at: indexPath.row)
        updateView(state: .update)
    }
}

//MARK:- UIAdaptivePresentationControllerDelegate Methods

extension BasketViewModel: UIAdaptivePresentationControllerDelegate {
    
    func presentationControllerDidDismiss(_ presentationController: UIPresentationController) {
        closeView()
    }
}
