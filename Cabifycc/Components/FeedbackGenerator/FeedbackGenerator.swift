//
//  FeedbackGenerator.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 29/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

protocol FeedbackGenerating {
    func selectionDidChange()
    func rigidImpactOccurred()
    func heavyImpactOccurred()
}

final class FeedbackGenerator: FeedbackGenerating {
    
    let selection = UISelectionFeedbackGenerator()
    var rigidImpact = UIImpactFeedbackGenerator(style: .rigid)
    var heavyImpact = UIImpactFeedbackGenerator(style: .medium)

    func selectionDidChange() {
        selection.selectionChanged()
    }
    
    func rigidImpactOccurred() {
        rigidImpact.impactOccurred()
    }
    
    func heavyImpactOccurred() {
        heavyImpact.impactOccurred()
    }
}
