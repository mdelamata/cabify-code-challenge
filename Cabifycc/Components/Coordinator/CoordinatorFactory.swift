//
//  CoordinatorFactory.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

final class CoordinatorFactory: CoordinatorFactoryProtocol {
    
    func createListCoordinator(navigationController: UINavigationController) -> Coordinating {
        return ListCoordinator(navigationController: navigationController)
    }
    
    func createBasketCoordinator(navigationController: UINavigationController, with checkout: Checkout) -> Coordinating {
        return BasketCoordinator(navigationController: navigationController, checkout: checkout)
    }
}
