//
//  RootCoordinator.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

final class RootCoordinator: Coordinating {
    
    weak var parentCoordinator: Coordinating?
    var childCoordinators = [Coordinating]()
    var navigationController: UINavigationController
    
    var coordinatorFactory: CoordinatorFactoryProtocol = CoordinatorFactory()
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        navigationController.navigationBar.prefersLargeTitles = true
        createListViewController()
    }
    
    func childDidFinish(_ child: Coordinating) {
        for (index, coordinator) in childCoordinators.enumerated() {
            if child === coordinator {
                childCoordinators.remove(at: index)
            }
        }
    }
    
}


// MARK:- RootCoordinating Methods

extension RootCoordinator: RootCoordinating {

    func createListViewController() {
        let child = coordinatorFactory.createListCoordinator(navigationController: navigationController)
        childCoordinators.append(child)
        child.parentCoordinator = self
        child.start()
    }
    
    func createBasketViewController(checkout: Checkout) {
        let child = coordinatorFactory.createBasketCoordinator(navigationController: navigationController,
                                                               with: checkout)
        childCoordinators.append(child)
        child.parentCoordinator = self
        child.start()
    }
    
    func updateCheckout(_ checkout: Checkout) {
        (childCoordinators.filter{ $0 is ListCoordinating }
            .first as? ListCoordinating)?
            .updateCheckout(checkout)
    }
}
