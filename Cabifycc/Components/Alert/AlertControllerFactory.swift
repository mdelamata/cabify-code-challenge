//
//  AlertControllerFactory.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 26/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

struct AlertControllerFactory {
    
    static func createAlertController(viewModel: AlertViewModel) -> UIAlertController {
        
        let alert = UIAlertController(title: viewModel.title,
                                      message: viewModel.message,
                                      preferredStyle: .alert)
        
        if let dismiss = viewModel.dismiss {
            alert.addAction(UIAlertAction(title: dismiss,
                                          style: .cancel,
                                          handler: nil))
        }
        
        if let actions = viewModel.actions {
            
            actions.forEach { action in
                
                for (title, closure) in action {
                    alert.addAction(UIAlertAction(title: title,
                                                  style: .default,
                                                  handler: {  _ in closure() }))
                }
            }
            
        }
        
        return alert
    }
}
