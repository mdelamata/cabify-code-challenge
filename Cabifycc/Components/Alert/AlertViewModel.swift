//
//  AlertViewModel.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 26/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import Foundation

struct AlertViewModel {
    
    var title: String?
    var message: String?
    var dismiss: String?
    var actions: [[String: (()->())]]?
}
