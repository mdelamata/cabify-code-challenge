//
//  Checkout.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 26/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import Foundation

struct Checkout {
    
    var items: [CheckoutItem]
    
    var total: Float {
        items.reduce(0) { $0 + $1.afterPromotionPrice }
    }
    
    var savings: Float {
        items.reduce(0) { $0 + $1.beforePromotionPrice } - total
    }
    
    var productsQuantity: Int {
        items.reduce(0) { $0 + $1.quantity }
    }
}

extension Checkout {
    static func empty() -> Checkout {
        Checkout(items: [])
    }
}

extension Checkout: Equatable {
    static func == (lhs: Checkout, rhs: Checkout) -> Bool {
        return lhs.items == rhs.items
    }
}
