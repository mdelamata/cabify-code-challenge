//
//  CheckoutItem.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 26/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import Foundation

struct CheckoutItem {
    
    var product: Product
    var quantity: Int
    
    var beforePromotionPrice: Float {
        product.price * Float(quantity)
    }
    
    var afterPromotionPrice: Float {
        
        guard let promotion = product.promotion else {
            return beforePromotionPrice
        }
        
        switch promotion.typeCode {
        case .twoForOne:
            return apply2for1()
        case .reducedPrice:
            return reducedPrice(promotion: promotion)
        case .unknown:
            return beforePromotionPrice
        }
    }
    
    private func apply2for1() -> Float {
        let division = quantity.quotientAndRemainder(dividingBy: 2)
        return product.price * Float(division.quotient) + product.price * Float(division.remainder)
    }
    
    private func reducedPrice(promotion: Promotion) -> Float {
        
        guard let minimumQuantity = promotion.minimumQuantity, let reducedPrice = promotion.reducedPrice
        else { return beforePromotionPrice }
        
        if quantity >= minimumQuantity {
            return reducedPrice * Float(quantity)
        } else {
            return beforePromotionPrice
        }
    }
}

extension CheckoutItem: Equatable {
    static func == (lhs: CheckoutItem, rhs: CheckoutItem) -> Bool {
        return lhs.product == rhs.product && lhs.quantity == rhs.quantity
    }
}
