//
//  PromotionList.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import Foundation

struct PromotionList {
    let promotions: [Promotion]
}

extension PromotionList: Decodable {}
