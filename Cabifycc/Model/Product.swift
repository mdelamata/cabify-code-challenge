//
//  Product.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import Foundation

struct Product {
    var code: String
    var name: String
    var price: Float
    var promotion: Promotion?
    
    mutating func updatePromotion(promotion: Promotion) {
        self.promotion = promotion
    }
}

extension Product: Decodable {}

extension Product: Hashable {
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(code.hashValue)
    }

    static func == (lhs: Product, rhs: Product) -> Bool {
        return lhs.code == rhs.code && lhs.name == rhs.name && lhs.price == rhs.price
    }
}
