//
//  Checkout+Conversion.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 26/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import Foundation

extension Checkout {
    
    var toBasket: Basket {
        
        var basket = Basket()
        
        for item in items {
            basket[item.product] = item.quantity
        }
        
        return basket
    }
}
