//
//  Basket+Conversion.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 26/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import Foundation

extension Basket {
    
    var toCheckout: Checkout {
        
        var checkoutItems = [CheckoutItem]()
        
        for (product, amount) in self {
            checkoutItems.append(CheckoutItem(product: product, quantity: amount))
        }
        
        return Checkout(items: checkoutItems)
    }
}
