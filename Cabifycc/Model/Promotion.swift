//
//  Promotion.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import Foundation

enum PromotionCodeType: String, Codable {
    case twoForOne
    case reducedPrice
    case unknown
}

struct Promotion {
    var productCode: String
    var description: String
    var minimumQuantity: Int?
    var reducedPrice: Float?
    var typeCode: PromotionCodeType = .unknown
}

extension Promotion: Decodable {
    
    enum CodingKeys: String, CodingKey {
        case productCode, description, minimumQuantity, reducedPrice, typeCode
    }
}

