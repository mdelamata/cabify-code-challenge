//
//  Resource+ProductList.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import Foundation

extension Resource {

    static func products() -> Resource<ProductList> {
        let url = APIConstants.baseUrl.appendingPathComponent("/bins/4bwec")
        return Resource<ProductList>(url: url)
    }
}
