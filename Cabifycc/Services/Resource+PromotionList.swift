//
//  Resource+PromotionsList.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import Foundation

extension Resource {

    static func promotions() -> Resource<PromotionList> {
        let url = APIConstants.baseUrl.appendingPathComponent("/bins/veb0i")
        return Resource<PromotionList>(url: url)
    }
}
