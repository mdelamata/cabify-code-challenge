//
//  PurchaseService.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import Foundation
import Combine
import Reachability

// This service is returning Error or True depending on the internet connection
// but I wanted to build the purchase service so it's not just a button that
// shows an alert when you tap buy in the Basket View Controller
// Once we have a service this could be easily replaced by another Resource
// and a call to the real endpoint

struct PurchaseService: PurchaseServing {
    
    let reachability = try! Reachability()
    
    func pay(checkout: Checkout) -> AnyPublisher<Bool, Error> {
        
        let result: Result<Bool, Error> = {
            switch reachability.connection {
            case .unavailable:
                return .failure(NetworkError.noInternetError)
            default:
                return .success(true)
            }
        }()
        
        return result.publisher
            .subscribe(on: DispatchQueue.global())
            .receive(on: RunLoop.main)
            .delay(for: 0.3, scheduler: RunLoop.main)
            .eraseToAnyPublisher()
    }
}
