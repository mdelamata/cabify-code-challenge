//
//  ProductsService.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import Foundation
import Combine

struct ProductsService: ProductsServing {
    
    var networkService: NetworkServiceProtocol = NetworkService()
    
    func fetchProducts() -> AnyPublisher<ProductList, Error> {
        
        return networkService.load(Resource<ProductList>.products())
            .subscribe(on: DispatchQueue.global())
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
    
    func fetchPromotions() -> AnyPublisher<PromotionList, Error> {
        
        return networkService.load(Resource<PromotionList>.promotions())
            .subscribe(on: DispatchQueue.global())
            .receive(on: RunLoop.main)
            .eraseToAnyPublisher()
    }
}
