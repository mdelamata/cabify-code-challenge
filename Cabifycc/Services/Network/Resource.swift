//
//  Resource.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import Foundation

struct Resource<T: Decodable> {
    
    let url: URL
    let parameters: [String: CustomStringConvertible]?
   
    var request: URLRequest? {
        
        guard var components = URLComponents(url: url, resolvingAgainstBaseURL: false) else {
            return nil
        }
        
        if let parameters = parameters {
            components.queryItems = parameters.keys.map { key in
                URLQueryItem(name: key, value: parameters[key]?.description)
            }
        }
        
        guard let url = components.url else {
            return nil
        }
        
        return URLRequest(url: url)
    }

    init(url: URL, parameters: [String: CustomStringConvertible]? = nil) {
        self.url = url
        self.parameters = parameters
    }
}
