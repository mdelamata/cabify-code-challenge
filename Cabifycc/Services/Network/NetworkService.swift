//
//  NetworkService.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import Foundation
import Combine

final class NetworkService: NetworkServiceProtocol {
    
    var session: URLSession
    
    init(session: URLSession = URLSession(configuration: .default)) {
        self.session = session
    }
    
    @discardableResult
    func load<T>(_ resource: Resource<T>) -> AnyPublisher<T, Error> {
        
        guard let request = resource.request else {
            return Fail(error: NetworkError.requestError).eraseToAnyPublisher()
        }
        
        return session.dataTaskPublisher(for: request)
            .map { $0.data }
            .decode(type: T.self, decoder: JSONDecoder())
            .mapError( { error -> Error in
                
                switch (error as NSError).code {
                case -1009:
                    return NetworkError.noInternetError
                default:
                    break
                }
                
                if error is DecodingError {
                    return NetworkError.jsonDecodingError
                }
                
                return error
            })
            .eraseToAnyPublisher()
    }
    
}
