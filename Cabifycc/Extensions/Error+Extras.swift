//
//  Error+Extras.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import Foundation

public func areEqual<T: Error>(_ lhs: T, _ rhs: T) -> Bool {
    
    guard String(describing: lhs) == String(describing: rhs) else {
        return false
    }
    
    let nsl = lhs as NSError
    let nsr = rhs as NSError
    return nsl.isEqual(nsr)
}
