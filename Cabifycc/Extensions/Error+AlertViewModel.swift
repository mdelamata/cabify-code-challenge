//
//  Error+AlertViewModel.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 26/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import Foundation

extension Error {
    
    var toAlertViewModel: AlertViewModel {
        
        var errorMessage = "error.generic"

        if let networkError = self as? NetworkError {
            switch networkError {
            case .noInternetError:
                errorMessage = "error.no_internet"
            default:
                break
            }
        }
        
        return AlertViewModel(title: errorMessage.localized,
                              message: nil,
                              dismiss: "Dismiss",
                              actions: nil)
    }
}
