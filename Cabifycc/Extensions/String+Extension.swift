//
//  String+Extension.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 26/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

extension String {
    
    var strikeThroughed : NSMutableAttributedString {
        
        let attributeString =  NSMutableAttributedString(string: self)
        attributeString.addAttribute(NSAttributedString.Key.strikethroughStyle, value: 2, range: NSMakeRange(0, attributeString.length))
        return attributeString
    }
    
    var localized: String {
        get {
            return NSLocalizedString(self, comment: "")
        }
    }
    
}
