//
//  UIStoryboard+Extension.swift
//  Cabifycc
//
//  Created by Manuel de la Mata Sáez on 25/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    public static var main: UIStoryboard {
        return UIStoryboard(name: "Main", bundle: nil)
    }
}
