//
//  CheckoutItemTableViewCellTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 29/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import XCTest
import UIKit

class CheckoutItemTableViewCellTests: XCTestCase {
    
    func test_reusableIdentifier_isCorrect() {
        XCTAssertEqual(CheckoutItemTableViewCell.reusableIdentifier, "checkoutItemCell")
    }
    
    func test_configure_configuredCorrectly() {
        
        let cell = checkoutItemCell()
        
        let viewModel = TestUtils.someCheckoutItemCellViewModel
        
        cell.configure(viewModel: viewModel)
        
        XCTAssertEqual(cell.productImageView.image, viewModel.image)
        XCTAssertEqual(cell.nameLabel.text, viewModel.nameText)
        XCTAssertEqual(cell.quantityLabel.text, viewModel.quantityText)
        XCTAssertEqual(cell.beforePromotionPriceLabel.text, viewModel.beforePromotionPriceText)
        XCTAssertEqual(cell.afterPromotionPriceLabel.text, viewModel.afterPromotionPriceText)
    }
    
    func checkoutItemCell() -> CheckoutItemTableViewCell {
        
        let checkout = Checkout(items: [CheckoutItem(product: TestUtils.someProductB, quantity: 2)])
        
        let navigationController = UINavigationController()
        let basketInteractor = BasketInteractor()
        let basketCoordinator = BasketCoordinator(navigationController: navigationController,
                                              checkout: checkout)
        
        let viewModel = BasketViewModel(coordinator: basketCoordinator,
                                    interactor: basketInteractor,
                                    checkout: checkout)
        
        guard
            let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "BasketViewControllerNavigationController") as? UINavigationController,
            let basketViewController = navController.viewControllers.first as? BasketViewController
            else {
                XCTFail()
                return CheckoutItemTableViewCell()
        }
        
        basketViewController.viewModel = viewModel
        
        _ = basketViewController.view
        
        return viewModel.basketTableViewDataSource.tableView(basketViewController.tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as! CheckoutItemTableViewCell
    }
    
}
