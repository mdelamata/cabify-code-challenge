//
//  BasketInteractorTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import UIKit
import XCTest
import Combine

class BasketInteractorTests: XCTestCase {

    var basketInteractor: BasketInteractor! = BasketInteractor()
    
    override func tearDown() {
        basketInteractor = nil
        super.tearDown()
    }
    
    // MARK: - fetchItems
    
    func test_purchase_callsPurchaseService() {
        
        let checkout = Checkout.empty()
        
        let mockPurchaseService = MockPurchaseService()
        mockPurchaseService.payCheckoutResultToReturn = .success(true)
        
        basketInteractor.purchaseService = mockPurchaseService
        
        _ = basketInteractor.purchase(checkout: checkout)
        
        let receivedCheckout = try? XCTUnwrap(mockPurchaseService.payCheckoutWasCalledWithCheckout)
        XCTAssert(receivedCheckout == checkout)
    }
}
