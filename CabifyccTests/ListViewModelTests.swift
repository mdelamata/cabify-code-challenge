//
//  ListViewModelTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import XCTest
import Combine
import UIKit

class ListViewModelTests: XCTestCase {
    
    var viewModel: ListViewModel!
    var navigationController: UINavigationController!
    var listCoordinator: ListCoordinator!
    var listInteractor: ListInteractor! = ListInteractor()
    
    var renderAction: ((ListViewState) -> ())?
    
    override func setUp() {
        super.setUp()
        navigationController = UINavigationController()
        listCoordinator = ListCoordinator(navigationController: navigationController)
        listInteractor = ListInteractor()
        
        viewModel = ListViewModel(coordinator: listCoordinator, interactor: listInteractor)
    }
    
    override func tearDown() {
        navigationController = nil
        listCoordinator = nil
        listInteractor = nil
        viewModel = nil
        renderAction = nil
        
        super.tearDown()
    }
    
    //MARK: - Init
    
    func test_init_callsSubscribe() {
        XCTAssertNotNil(viewModel.interactorStateSubscriber)
    }
    
    //MARK: - updateView
    
    func test_updateView_forwardsStateToRenderAction() {
        
        var receivedState: ListViewState?
        let stateSent = ListViewState.loading
        
        viewModel.renderAction = { state in
            receivedState = state
        }
        
        viewModel.updateView(state: stateSent)
        
        XCTAssertEqual(receivedState, stateSent)
    }
    
    //MARK: - ListViewModelProtocol Methods
    
    //MARK: - configureTableView
    
    func test_configureTableView_createsDataSourceAndDelegateClassesAndMakesAssignations() {
        
        let tableView = UITableView()
        viewModel.configure(tableView: tableView)
        
        XCTAssertNotNil(viewModel.listTableViewDataSource)
        XCTAssertNotNil(viewModel.listTableViewDelegate)
        XCTAssert(tableView.dataSource === viewModel.listTableViewDataSource)
        XCTAssert(tableView.delegate === viewModel.listTableViewDelegate)
    }
    
    //MARK: - fetchItems
    
    func test_fetchItems_callsInteractorFetchItems() {
        
        let mockListInteractor = MockListInteractor()
        viewModel.interactor = mockListInteractor
        
        viewModel.fetchItems()
        
        XCTAssertTrue(mockListInteractor.fetchItemsWasCalled)
    }
    
    // MARK: - viewBasketTapped
    
    func test_viewBasketTapped_callsCoordinator() {
        
        let mockCoordinator = MockCoordinator(navigationController: navigationController)
        viewModel.coordinator = mockCoordinator
        
        viewModel.viewBasketTapped()
        
        let sentCheckout = try? XCTUnwrap(mockCoordinator.showBasketViewWithCheckoutWasCalledWith)
        XCTAssertEqual(listInteractor.checkout.value, sentCheckout)
    }
    
    // MARK: - updateBasket
    
    func test_updateBasket_callsInteractorUpdateBasket() {
        
        let mockListInteractor = MockListInteractor()
        viewModel.interactor = mockListInteractor
        
        let basket = Basket()
        viewModel.updateBasket(basket: basket)
        
        let receivedBasket = try? XCTUnwrap(mockListInteractor.updateBasketWasCalledWith)
        XCTAssertEqual(receivedBasket, basket)
    }
    
    func test_updateBasket_callsUpdateViewWithUpdateState() {
        
        var receivedState: ListViewState?
        
        viewModel.renderAction = { state in
            receivedState = state
        }
        
        viewModel.updateBasket(basket: Basket())
        
        XCTAssertEqual(receivedState, .update)
    }
    
    // MARK: - setBasketProduct:WithQuantity:
    
    func test_setBasketProductWithQuantity_callsInteractorSetBasketProductWithQuantity() {
        
        let mockListInteractor = MockListInteractor()
        viewModel.interactor = mockListInteractor
        
        let product = TestUtils.someProductA
        let quantity = 2
        viewModel.setBasket(product: product, withQuantity: quantity)
        
        let receivedTuple = try? XCTUnwrap(mockListInteractor.setBasketWasCalledWith)
        XCTAssertEqual(receivedTuple?.0, product)
        XCTAssertEqual(receivedTuple?.1, quantity)
    }
    
    func test_setBasketProductWithQuantity_callsUpdateViewWithUpdateState() {
        
        var receivedState: ListViewState?
        
        viewModel.renderAction = { state in
            receivedState = state
        }
        
        viewModel.setBasket(product: TestUtils.someProductA, withQuantity: 1)
        
        XCTAssertEqual(receivedState, .update)
    }
    
    // MARK: - basketQuantityForProduct:
    
    func test_basketQuantityForProduct_callsInteractorBasketQuantity() {
        
        let mockListInteractor = MockListInteractor()
        viewModel.interactor = mockListInteractor
        
        let product = TestUtils.someProductA
        let quantity = 2
        
        mockListInteractor.expectedBasketQuantityForProduct = quantity
        
        let receivedQuantity = viewModel.basketQuantity(for: product)
        
        let receivedProduct = try? XCTUnwrap(mockListInteractor.basketQuantityForProductWasCalledWith)
        XCTAssertEqual(receivedProduct, product)
        XCTAssertEqual(receivedQuantity, quantity)
    }
    
    // MARK: - shouldShowViewBasket:
    
    func test_shouldShowViewBasket_checkoutWithProducts_shouldReturnTrue() {
        
        let checkout = Checkout(items: [CheckoutItem(product: TestUtils.someProductA, quantity: 2)])
        listInteractor.checkout.send(checkout)
        
        XCTAssertTrue(viewModel.shouldShowViewBasket())
    }
    
    func test_shouldShowViewBasket_checkoutEmpty_shouldReturnFalse() {
        
        listInteractor.checkout.send(Checkout.empty())
        
        XCTAssertFalse(viewModel.shouldShowViewBasket())
    }
    
    // MARK: - productsCount:
    
    func test_productsCount_checkoutWithProducts_returnsCorrectString() {
        
        let quantity = 2
        let checkout = Checkout(items: [CheckoutItem(product: TestUtils.someProductA, quantity: quantity)])
        listInteractor.checkout.send(checkout)
        
        XCTAssert(viewModel.productsCount() == String(quantity))
    }
    
    func test_productsCount_checkoutEmptu_returnsCorrectString() {
        
        let quantity = 0
        listInteractor.checkout.send(Checkout.empty())
        
        XCTAssert(viewModel.productsCount() == String(quantity))
    }
    
    // MARK: - totalPrice:
    
    func test_totalPrice_retursCorrectFormattedPrice() {
        
        let product = TestUtils.someProductC
        let quantity = 2
        let checkout = Checkout(items: [CheckoutItem(product: product, quantity: quantity)])
        listInteractor.checkout.send(checkout)
        
        let expectedPriceString = "$" +  String(format: "%.2f", (product.price * Float(quantity)))
        
        XCTAssert(viewModel.totalPrice() == expectedPriceString)
        
    }
    
    //MARK:- ListViewModelCellViewModelProtocol Methods
    
    //MARK:- getCellViewModel
    
    func test_getCellViewModel_callsFactoryWithCorrectItem() {
        
        let product = TestUtils.someProductA
        let quantity = 2
        
        listInteractor.products.send([product])
        listInteractor.setBasket(product: product, withQuantity: quantity)
        
        let mockFactory = MockProductCellViewModelFactory()
        mockFactory.expectedCellViewModel = TestUtils.someProductCellViewModel
        
        viewModel.factory = mockFactory
        
        let indexPath = IndexPath(row: 0, section: 0)
        
        _ = viewModel.getCellViewModel(at: indexPath)
        
        let receivedTuple = try? XCTUnwrap(mockFactory.createProductCellViewModelWasCalledWith)
        XCTAssertEqual(receivedTuple?.0, product)
        XCTAssertEqual(receivedTuple?.1, quantity)
    }
    
    //MARK:- numberOfItems
    
    func test_numberOfItems_returnsInteractorProductsCount() {
        
        let products = [TestUtils.someProductA, TestUtils.someProductB]
        listInteractor.products.send(products)
        
        let count = viewModel.numberOfItems()
        
        XCTAssertEqual(products.count, count)
    }
    
    //MARK:- didSelectRow
    
    func test_didSelectRow_quantityGreaterThanOne_callsCoordinatorWithCorrectQuantity() {
        
        let mockCoordinator = MockCoordinator(navigationController: navigationController)
        viewModel.coordinator = mockCoordinator
        
        let product = TestUtils.someProductA
        let quantity = 2
        
        listInteractor.products.send([product])
        listInteractor.setBasket(product: product, withQuantity: quantity)
        
        let indexPath = IndexPath(row: 0, section: 0)

        viewModel.didSelectRow(at: indexPath)
        
        let receivedTuple = try? XCTUnwrap(mockCoordinator.showDetailProductWithCurrentQuantityWasCalledWith)
        XCTAssertEqual(receivedTuple?.0, product)
        XCTAssertEqual(receivedTuple?.1, quantity)
    }
    
    func test_didSelectRow_quantityZero_callsCoordinatorWithQuantityOne() {
    
          let mockCoordinator = MockCoordinator(navigationController: navigationController)
          viewModel.coordinator = mockCoordinator
          
          let product = TestUtils.someProductA
          let quantity = 0
          
          listInteractor.products.send([product])
          listInteractor.setBasket(product: product, withQuantity: quantity)
          
          let indexPath = IndexPath(row: 0, section: 0)

          viewModel.didSelectRow(at: indexPath)
          
          let receivedTuple = try? XCTUnwrap(mockCoordinator.showDetailProductWithCurrentQuantityWasCalledWith)
          XCTAssertEqual(receivedTuple?.0, product)
          XCTAssertEqual(receivedTuple?.1, 1)
      }
}
