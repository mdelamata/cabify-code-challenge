//
//  MockNetworkService.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 29/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import XCTest
import Combine

final class MockNetworkService<T>: NetworkServiceProtocol where T: Decodable  {
    
    var loadWasCalled = false
    var resultToReturn: Result<T, Error>?
    
    func load<T>(_ resource: Resource<T>) -> AnyPublisher<T, Error> where T : Decodable {
        loadWasCalled = true
        return resultToReturn?.publisher.eraseToAnyPublisher() as! AnyPublisher<T, Error>
    }
}
