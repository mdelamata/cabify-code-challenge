//
//  MockPurchaseService.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import Combine

class MockPurchaseService: PurchaseServing {
    
    var payCheckoutWasCalledWithCheckout: Checkout?
    var payCheckoutResultToReturn: Result<Bool, Error>?

    func pay(checkout: Checkout) -> AnyPublisher<Bool, Error> {
        payCheckoutWasCalledWithCheckout = checkout
        return payCheckoutResultToReturn!.publisher.eraseToAnyPublisher()
    }
}
