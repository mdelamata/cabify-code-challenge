//
//  MockNavigationController.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

final class MockNavigationController: UINavigationController {
    
    var dismissWasCalled = false
    var presentedViewControllerToReturn: UIViewController?

    override func dismiss(animated flag: Bool, completion: (() -> Void)? = nil) {
        dismissWasCalled = true
        completion?() //calls completion immediately for testing purposes
    }
    
    override var presentedViewController: UIViewController? {
        presentedViewControllerToReturn
    }
    
}
