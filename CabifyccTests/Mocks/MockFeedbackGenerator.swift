//
//  MockFeedbackGenerator.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 29/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import XCTest
import UIKit

final class MockFeedbackGenerator: FeedbackGenerating {
    
    var selectionDidChangeWasCalled = false
    var rigidImpactOccurredWasCalled = false
    var heavyImpactOccurredWasCalled = false
    
    func selectionDidChange() {
        selectionDidChangeWasCalled = true
    }
    
    func rigidImpactOccurred() {
        rigidImpactOccurredWasCalled = true
    }
    
    func heavyImpactOccurred() {
        heavyImpactOccurredWasCalled = true
    }
}
