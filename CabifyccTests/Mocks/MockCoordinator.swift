//
//  MockCoordinator.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import UIKit

final class MockCoordinator: Coordinating {
    
    var parentCoordinator: Coordinating?
    var childCoordinators: [Coordinating] = []
    var navigationController: UINavigationController
    var startWasCalled = false
    var childDidFinishWasCalledWithCoordinator: Coordinating?
    var createBasketViewControllerWasCalledWithCheckout: Checkout?
    var updateCheckoutWasCalledWithCheckout: Checkout?
    var showBasketViewWithCheckoutWasCalledWith: Checkout?
    var showDetailProductWithCurrentQuantityWasCalledWith: (Product, Int)?
    var dissmissDetailProductWithNewQuantityWasCalledWith: (Product, Int?)?
    var closeBasketViewWithCheckoutWasCalledWith: Checkout?
    var showAlertWasCalledWithAlertViewModel: AlertViewModel?
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    func start() {
        startWasCalled = true
    }
 
    func childDidFinish(_ child: Coordinating) {
        childDidFinishWasCalledWithCoordinator = child
    }
}

extension MockCoordinator: ListCoordinating {
    
    func showBasketView(checkout: Checkout) {
        showBasketViewWithCheckoutWasCalledWith = checkout
    }
    
    func showDetail(product: Product, currentQuantity: Int) {
        showDetailProductWithCurrentQuantityWasCalledWith = (product, currentQuantity)
    }
    
    func dismissDetail(product: Product, newQuantity: Int?) {
        dissmissDetailProductWithNewQuantityWasCalledWith = (product, newQuantity)
    }
    
    func updateCheckout(_ checkout: Checkout) {
        updateCheckoutWasCalledWithCheckout = checkout
    }
}

extension MockCoordinator: BasketCoordinating {
    
    func closeBasket(checkout: Checkout) {
        closeBasketViewWithCheckoutWasCalledWith = checkout
    }
}

extension MockCoordinator: RootCoordinating {
    
    func createListViewController() {}
    
    func createBasketViewController(checkout: Checkout) {
        createBasketViewControllerWasCalledWithCheckout = checkout
    }
}

extension MockCoordinator: Alerting {
    
    func showAlert(viewModel: AlertViewModel) {
        showAlertWasCalledWithAlertViewModel = viewModel
    }
}
