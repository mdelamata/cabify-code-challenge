//
//  MockListViewModel.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import UIKit

class MockListViewModel: ListViewModelProtocol {
    
    var viewBasketTappedWasCalled = false
    var updateBasketWasCalledWithBasket: Basket?
    var setBasketWithQuantityWasCalledWith: (Product, Int)?
    var basketQuantityForProductWasCalledWithProduct: Product?
    var expectedBasketQuantityForProduct: Int!
    var configureTableViewWasCalledWithTableView: UITableView?
    var fetchItemsWasCalled = false
    var shouldShowViewBasketWasCalled = false
    var expectedShouldShowViewBasket: Bool!
    var productsCountWasCalled = false
    var expectedProductsCount: String! = ""
    var totalPriceWasCalled = false
    var expectedTotalPrice: String! = ""
    var titleWasCalled = false
    var expectedTitle: String! = ""
    var viewBasketTitleWasCalled = false
    var expectedViewBasketTitle: String! = ""

    var getCellViewModelAtIndexPathWasCalledWith: IndexPath?
    var expectedProductCellViewModelToReturn: ProductCellViewModel!
    var numberOfItemsWasCalled = false
    var expectedNumberOfItemsWasCalledToReturn: Int! = 0
    var didSelectRowAtIndexPathWasCalledWith: IndexPath?

    
    var renderAction: ((ListViewState) -> ())?

    
    func viewBasketTapped() {
        viewBasketTappedWasCalled = true
    }
    
    func updateBasket(basket: Basket) {
        updateBasketWasCalledWithBasket = basket
    }
    
    func setBasket(product: Product, withQuantity quantity: Int) {
        setBasketWithQuantityWasCalledWith = (product, quantity)
    }
    
    func basketQuantity(for product: Product) -> Int {
        basketQuantityForProductWasCalledWithProduct = product
        return expectedBasketQuantityForProduct
    }
    
    func configure(tableView: UITableView) {
        configureTableViewWasCalledWithTableView = tableView
    }
    
    func fetchItems() {
        fetchItemsWasCalled = true
    }
    
    func shouldShowViewBasket() -> Bool {
        shouldShowViewBasketWasCalled = true
        return expectedShouldShowViewBasket
    }
    
    func productsCount() -> String {
        productsCountWasCalled = true
        return expectedProductsCount
    }
    
    func totalPrice() -> String {
        totalPriceWasCalled = true
        return expectedTotalPrice
    }
    
    func title() -> String {
        titleWasCalled = true
        return expectedTitle
    }
    
    func viewBasketTitle() -> String {
        viewBasketTitleWasCalled = true
        return expectedViewBasketTitle
    }
}

extension MockListViewModel: CellListViewModelProtocol {
    
    func getCellViewModel(at indexPath: IndexPath) -> ProductCellViewModel {
        getCellViewModelAtIndexPathWasCalledWith = indexPath
        return expectedProductCellViewModelToReturn
    }
    
    func numberOfItems() -> Int {
        numberOfItemsWasCalled = true
        return expectedNumberOfItemsWasCalledToReturn
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        didSelectRowAtIndexPathWasCalledWith = indexPath
    }
}
