//
//  MockProductDetailViewModel.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import XCTest

class MockProductDetailViewModel: ProductDetailViewModelProtocol {
    
    var title: String = ""
    var price: String = ""
    var descriptionItem: String  = ""
    var currentQuantity: String  = ""
    var chooseQuantity: String  = ""
    var addToBasket: String  = ""
    
    var increaseItemQuantityWasCalled = false
    var decreaseItemQuantityWasCalled = false
    var addItemsToBasketWasCalled = false
    var dismissWasCalled = true
    
    var renderAction: ((ProductDetailState) -> ())?

    func increaseItemQuantity() {
        increaseItemQuantityWasCalled = true
    }
    
    func decreaseItemQuantity() {
        decreaseItemQuantityWasCalled = true
    }
    
    func addItemsToBasket() {
        addItemsToBasketWasCalled = true
    }
    
    func dismiss() {
        dismissWasCalled = true
    }
}

