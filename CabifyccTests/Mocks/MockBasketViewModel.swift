//
//  MockBasketViewModel.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import UIKit

class MockBasketViewModel: BasketViewModelProtocol {
    
    var titleWasCalled = false
    var orderValueWasCalled = false
    var orderValueAmountWasCalled = false
    var savingsWasCalled = false
    var savingsAmountWasCalled = false
    var totalWasCalled = false
    var totalAmountWasCalled = false
    var payButtonTitleWasCalled = false
    var payButtonEnabledWasCalled = false
    var expectedPayButtonEnabled: Bool! = false
    var purchaseAlertTitleWasCalled = false
    var purchaseAlertDismissWasCalled = false
    var payTappedWasCalled = false
    var closeViewWasCalled = false
    var configureTableViewWasCalledWithTableView: UITableView!
    
    var renderAction: ((BasketViewState) -> ())?
    
    var stringToReturn = ""
    
    var getCellViewModelAtIndexPathWasCalledWith: IndexPath?
    var expectedCheckoutItemCellViewModelToReturn: CheckoutItemCellViewModel!
    var numberOfItemsWasCalled = false
    var expectedNumberOfItemsWasCalledToReturn: Int! = 0
    var didSelectRowAtIndexPathWasCalledWith: IndexPath?
    var didDeleteRowAtIndexPathWasCalledWith: IndexPath?
    
    func title() -> String {
        titleWasCalled = true
        return stringToReturn
    }
    
    func orderValue() -> String {
        orderValueWasCalled = true
        return stringToReturn
    }
    
    func orderValueAmount() -> String {
        orderValueAmountWasCalled = true
        return stringToReturn
    }
    
    func savings() -> String {
        savingsWasCalled = true
        return stringToReturn
    }
    
    func savingsAmount() -> String {
        savingsAmountWasCalled = true
        return stringToReturn
    }
    
    func total() -> String {
        totalWasCalled = true
        return stringToReturn
    }
    
    func totalAmount() -> String {
        totalAmountWasCalled = true
        return stringToReturn
    }
    
    func payButtonTitle() -> String {
        payButtonTitleWasCalled = true
        return stringToReturn
    }
    
    func payButtonEnabled() -> Bool {
        payButtonEnabledWasCalled = true
        return expectedPayButtonEnabled
    }
    
    func purchaseAlertTitle() -> String {
        purchaseAlertTitleWasCalled = true
        return stringToReturn
    }
    
    func purchaseAlertDismiss() -> String {
        purchaseAlertDismissWasCalled = true
        return stringToReturn
    }
    
    func payTapped() {
        payTappedWasCalled = true
    }
    
    func closeView() {
        closeViewWasCalled = true
    }
    
    func configure(tableView: UITableView) {
        configureTableViewWasCalledWithTableView = tableView
    }
}

extension MockBasketViewModel: CellBasketViewModelProtocol {
    
    func getCellViewModel(at indexPath: IndexPath) -> CheckoutItemCellViewModel {
        getCellViewModelAtIndexPathWasCalledWith = indexPath
        return expectedCheckoutItemCellViewModelToReturn
    }
    
    func numberOfItems() -> Int {
        numberOfItemsWasCalled = true
        return expectedNumberOfItemsWasCalledToReturn
    }
    
    func didSelectRow(at indexPath: IndexPath) {
        didSelectRowAtIndexPathWasCalledWith = indexPath
        
    }
    
    func didDeleteRow(at indexPath: IndexPath) {
        didDeleteRowAtIndexPathWasCalledWith = indexPath
    }
}
