//
//  MockProductCellViewModelFactory.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc

class MockProductCellViewModelFactory: ProductCellViewModelFactoryProtocol {
    
    var createProductCellViewModelWasCalledWith: (Product, Int)?
    var expectedCellViewModel: ProductCellViewModel?
    
    func createProductCellViewModel(for product: Product, amount: Int) -> ProductCellViewModel {
        createProductCellViewModelWasCalledWith = (product, amount)
        return expectedCellViewModel!
    }
}
