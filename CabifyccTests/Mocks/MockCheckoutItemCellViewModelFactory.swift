//
//  MockCheckoutItemCellViewModelFactory.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc

class MockCheckoutItemCellViewModelFactory: CheckoutItemCellViewModelFactoryProtocol {
    
    var createCheckoutItemCellViewModelWasCalledWithCheckoutItem: CheckoutItem?
    var expectedCellViewModel: CheckoutItemCellViewModel?
    
    func createCheckoutItemCellViewModel(for checkoutItem: CheckoutItem) -> CheckoutItemCellViewModel {
        createCheckoutItemCellViewModelWasCalledWithCheckoutItem = checkoutItem
        return expectedCellViewModel!
    }
}
