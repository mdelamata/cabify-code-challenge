//
//  MockListInteractor.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import Combine

final class MockListInteractor: ListInteracting {
    
    let products: CurrentValueSubject<[Product], Never>
    let state: CurrentValueSubject<ListInteractorState, Never>
    let checkout: CurrentValueSubject<Checkout, Never>
    var basket = Basket()
    
    var updateBasketWasCalledWith: Basket?
    var setBasketWasCalledWith: (Product, Int)?
    var basketQuantityForProductWasCalledWith: Product?
    var expectedBasketQuantityForProduct: Int!
    var fetchItemsWasCalled = false

    init(initialProducts: [Product] = [],
         initialState: ListInteractorState = .idle,
         initialCheckout: Checkout = Checkout.empty()) {
        
        products = CurrentValueSubject<[Product], Never>(initialProducts)
        state = CurrentValueSubject<ListInteractorState, Never>(initialState)
        checkout = CurrentValueSubject<Checkout, Never>(initialCheckout)
    }
    
    func updateBasket(basket: Basket) {
        updateBasketWasCalledWith = basket
    }
    
    func setBasket(product: Product, withQuantity quantity: Int) {
        setBasketWasCalledWith = (product, quantity)
    }
    
    func basketQuantity(for product: Product) -> Int {
        basketQuantityForProductWasCalledWith = product
        return expectedBasketQuantityForProduct
    }
    
    func fetchItems() {
        fetchItemsWasCalled = true
    }
}
