//
//  MockProductTableViewCell.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import UIKit

class MockProductTableViewCell: ProductTableViewCell {
    
    var configureWasCalled = false
    
    override func configure(viewModel: ProductCellViewModel) {
        configureWasCalled = true
    }
}
