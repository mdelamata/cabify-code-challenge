//
//  MockTableView.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

class MockTableView: UITableView {
    
    var reloadDataWasCalled = false
    var dequeuedCellToReturn: UITableViewCell!
    var deselectRowWasCalledAtIndexPath: IndexPath?

    override func reloadData() {
        reloadDataWasCalled = true
    }
    
    override func dequeueReusableCell(withIdentifier identifier: String) -> UITableViewCell? {
        return dequeuedCellToReturn
    }
    
    override func deselectRow(at indexPath: IndexPath, animated: Bool) {
        deselectRowWasCalledAtIndexPath = indexPath
    }
}
