//
//  MockCheckoutItemTableViewCell.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 29/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import UIKit

class MockCheckoutItemTableViewCell: CheckoutItemTableViewCell {
    
    var configureWasCalled = false
    
    override func configure(viewModel: CheckoutItemCellViewModel) {
        configureWasCalled = true
    }
}
