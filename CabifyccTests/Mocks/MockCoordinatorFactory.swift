//
//  MockCoordinatorFactory.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import UIKit

struct MockCoordinatorFactory: CoordinatorFactoryProtocol {
    
    var coordinatorToReturn: MockCoordinator?

    func createListCoordinator(navigationController: UINavigationController) -> Coordinating {
        coordinatorToReturn!
    }
    
    func createBasketCoordinator(navigationController: UINavigationController, with checkout: Checkout) -> Coordinating {
        coordinatorToReturn!
    }
}
