//
//  MockProductService.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import Combine

class MockProductsService: ProductsServing {
    
    var fecthProductsWasCalled = false
    var fecthProductsResultToReturn: Result<ProductList, Error>?

    var fetchPromotionsWasCalled = false
    var fetchPromotionsResultToReturn: Result<PromotionList, Error>?

    func fetchProducts() -> AnyPublisher<ProductList, Error> {
        fecthProductsWasCalled = true
        return fecthProductsResultToReturn!.publisher.eraseToAnyPublisher()
    }
    
    func fetchPromotions() -> AnyPublisher<PromotionList, Error> {
        fetchPromotionsWasCalled = true
        return fetchPromotionsResultToReturn!.publisher.eraseToAnyPublisher()
    }
}
