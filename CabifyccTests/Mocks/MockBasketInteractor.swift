//
//  MockBasketInteractor.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import UIKit
import Combine

final class MockBasketInteractor: BasketInteracting {
    
    var purchaseCheckoutWasCalledWithCheckout: Checkout?
    var purchaseCheckoutResult: Result<Bool, Error>!
    
    func purchase(checkout: Checkout) -> AnyPublisher<Bool, Error> {
        purchaseCheckoutWasCalledWithCheckout = checkout
        return purchaseCheckoutResult.publisher.eraseToAnyPublisher()
    }
}
