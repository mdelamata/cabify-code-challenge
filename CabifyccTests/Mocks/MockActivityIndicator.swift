//
//  MockActivityIndicator.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import UIKit

class MockActivityIndicator: UIActivityIndicatorView {
    
    var isHiddenWasCalled = false
    var newIsHiddenValue: Bool!
    
    override var isHidden: Bool {
        didSet{
            isHiddenWasCalled = true
            newIsHiddenValue = isHidden
        }
    }
}
