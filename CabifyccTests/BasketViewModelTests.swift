//
//  BasketViewModelTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import XCTest
import Combine
import UIKit

class BasketViewModelTests: XCTestCase {
    
    var viewModel: BasketViewModel!
    var navigationController: UINavigationController!
    var basketCoordinator: BasketCoordinator!
    var basketInteractor: BasketInteractor!
    
    var checkout: Checkout!
    
    var renderAction: ((ListViewState) -> ())?
    
    override func setUp() {
        super.setUp()
        
        checkout = Checkout.empty()
        navigationController = UINavigationController()
        basketCoordinator = BasketCoordinator(navigationController: navigationController,
                                              checkout: checkout)
        basketInteractor = BasketInteractor()
        
        viewModel = BasketViewModel(coordinator: basketCoordinator,
                                    interactor: basketInteractor,
                                    checkout: checkout)
    }
    
    override func tearDown() {
        navigationController = nil
        basketCoordinator = nil
        basketInteractor = nil
        viewModel = nil
        renderAction = nil
        checkout = nil
        
        super.tearDown()
    }
    
    //MARK:- updateView
    
    func test_updateView_forwardsStateToRenderAction() {
        
        var receivedState: BasketViewState?
        let stateSent = BasketViewState.update
        
        viewModel.renderAction = { state in
            receivedState = state
        }
        
        viewModel.updateView(state: stateSent)
        
        XCTAssertEqual(receivedState, stateSent)
    }
    
    //MARK:- dismissView
    
    func test_dismissView_callsCoordinatorCloseBasket() {
        
        let mockCoordinator = MockCoordinator(navigationController: navigationController)
        viewModel.coordinator = mockCoordinator
        
        viewModel.dismissView()
        
        let receivedCheckout = try? XCTUnwrap(mockCoordinator.closeBasketViewWithCheckoutWasCalledWith)
        XCTAssertEqual(receivedCheckout, checkout)
    }
    
    //MARK:- purchaseDone
    
    func test_purchaseDone_callsCoordinatorShowAlert() {
        
        let mockCoordinator = MockCoordinator(navigationController: navigationController)
        viewModel.coordinator = mockCoordinator
        
        viewModel.purchaseDone()
        
        XCTAssertNotNil(mockCoordinator.showAlertWasCalledWithAlertViewModel)
    }
    
    func test_purchaseDone_callsFeedbackGenerator() {
        
        let mockFeedbackGenerator = MockFeedbackGenerator()
        viewModel.feedbackGenerator = mockFeedbackGenerator
        
        viewModel.purchaseDone()

        XCTAssertTrue(mockFeedbackGenerator.heavyImpactOccurredWasCalled)
    }
    
    // MARK: - BasketViewModelProtocol Methods
    
    // MARK: - configureTableView
    
    func test_configureTableView_createsDataSourceAndDelegateClassesAndMakesAssignations() {
        
        let tableView = UITableView()
        viewModel.configure(tableView: tableView)
        
        XCTAssertNotNil(viewModel.basketTableViewDataSource)
        XCTAssertNotNil(viewModel.basketTableViewDelegate)
        XCTAssert(tableView.dataSource === viewModel.basketTableViewDataSource)
        XCTAssert(tableView.delegate === viewModel.basketTableViewDelegate)
    }
    
    //MARK: - closeView
    
    func test_closeView_callsCoordinatorCloseBasketWithCurrentCheckout() {
        
        let mockCoordinator = MockCoordinator(navigationController: navigationController)
        viewModel.coordinator = mockCoordinator
        
        viewModel.closeView()
        
        let receivedCheckout = try? XCTUnwrap(mockCoordinator.closeBasketViewWithCheckoutWasCalledWith)
        XCTAssertEqual(receivedCheckout, checkout)
    }
    
    //MARK: - payTapped
    
    func test_payTapped_callsInteractorPurchase() {
        
        let mockBasketInteractor = MockBasketInteractor()
        mockBasketInteractor.purchaseCheckoutResult = .success(true)
        viewModel.interactor = mockBasketInteractor
        
        viewModel.payTapped()
        
        let receivedCheckout = try? XCTUnwrap(mockBasketInteractor.purchaseCheckoutWasCalledWithCheckout)
        XCTAssertEqual(receivedCheckout, checkout)
    }
    
    func test_payTapped_successfull_callsPurchaseDone() {
        
        let mockCoordinator = MockCoordinator(navigationController: navigationController)
        viewModel.coordinator = mockCoordinator
        
        let mockBasketInteractor = MockBasketInteractor()
        mockBasketInteractor.purchaseCheckoutResult = .success(true)
        viewModel.interactor = mockBasketInteractor
        
        viewModel.payTapped()
        
        let receivedAlertViewModel = try? XCTUnwrap(mockCoordinator.showAlertWasCalledWithAlertViewModel)

        XCTAssert(receivedAlertViewModel?.title == "Thanks for your purchase!")
    }
    
    func test_payTapped_failure_callsCoordinatorShowAlert() {
        
        let mockCoordinator = MockCoordinator(navigationController: navigationController)
        viewModel.coordinator = mockCoordinator
        
        let mockBasketInteractor = MockBasketInteractor()
        mockBasketInteractor.purchaseCheckoutResult = .failure(NetworkError.noInternetError)
        viewModel.interactor = mockBasketInteractor
        
        viewModel.payTapped()
        
        XCTAssertNotNil(mockCoordinator.showAlertWasCalledWithAlertViewModel)
    }
    
    //MARK: - Data
    
    func test_orderValueAmount_returnsCorrectString() {
        
        var product = TestUtils.someProductA
        product.updatePromotion(promotion: TestUtils.somePromotion)
        
        let checkout = Checkout(items: [CheckoutItem(product: product, quantity: 2)])
        viewModel.checkout = checkout
        
        XCTAssertEqual(viewModel.orderValueAmount(), "$10.00")
    }
    
    func test_savingsAmount_returnsCorrectString() {
        
        var product = TestUtils.someProductA
        product.updatePromotion(promotion: TestUtils.somePromotion)
        
        let checkout = Checkout(items: [CheckoutItem(product: product, quantity: 2)])
        viewModel.checkout = checkout
        
        XCTAssertEqual(viewModel.savingsAmount(), "$5.00")
    }
    
    func test_totalAmount_returnsCorrectString() {
        
        var product = TestUtils.someProductA
        product.updatePromotion(promotion: TestUtils.somePromotion)
        
        let checkout = Checkout(items: [CheckoutItem(product: product, quantity: 2)])
        viewModel.checkout = checkout
        
        XCTAssertEqual(viewModel.totalAmount(), "$5.00")
    }
    
    //MARK:- payButtonEnabled
    
    func test_payButtonEnabled_withNoItems_returnsFalse() {
        XCTAssertFalse(viewModel.payButtonEnabled())
    }
    
    func test_payButtonEnabled_withItems_returnsFalse() {
        
        var product = TestUtils.someProductA
        product.updatePromotion(promotion: TestUtils.somePromotion)
        
        let checkout = Checkout(items: [CheckoutItem(product: product, quantity: 2)])
        viewModel.checkout = checkout
        
        XCTAssertTrue(viewModel.payButtonEnabled())
    }
    
    //MARK:- CellBasketViewModelProtocol Methods
    
    //MARK:- getCellViewModel
    
    func test_getCellViewModel_callsFactoryWithCorrectItem() {

        let expectedCheckoutItem = CheckoutItem(product: TestUtils.someProductA, quantity: 2)
        
        viewModel.checkout = Checkout(items: [expectedCheckoutItem])
        
        let mockFactory = MockCheckoutItemCellViewModelFactory()
        mockFactory.expectedCellViewModel = TestUtils.someCheckoutItemCellViewModel
        
        viewModel.factory = mockFactory
        
        let indexPath = IndexPath(row: 0, section: 0)
        
        _ = viewModel.getCellViewModel(at: indexPath)
        
        let receivedCheckoutItem = try? XCTUnwrap(mockFactory.createCheckoutItemCellViewModelWasCalledWithCheckoutItem)
        XCTAssertEqual(receivedCheckoutItem, expectedCheckoutItem)
    }
    
    //MARK:- numberOfItems
    
    func test_numberOfItems_returnsInteractorProductsCount() {
        
        let items = [CheckoutItem(product: TestUtils.someProductA, quantity: 2),
                     CheckoutItem(product: TestUtils.someProductB, quantity: 2)]
        
        let checkout = Checkout(items: items)
        viewModel.checkout = checkout
        
        let count = viewModel.numberOfItems()
        
        XCTAssertEqual(checkout.items.count, count)
    }
    
    //MARK:- didDeleteRow

    func test_didDeleteRow_callsUpdateView() {
        
        let items = [CheckoutItem(product: TestUtils.someProductA, quantity: 2),
                     CheckoutItem(product: TestUtils.someProductB, quantity: 2)]
        
        let checkout = Checkout(items: items)
        viewModel.checkout = checkout
        
        var receivedState: BasketViewState?
        let stateSent = BasketViewState.update
        
        viewModel.renderAction = { state in
            receivedState = state
        }
        
        viewModel.didDeleteRow(at: IndexPath(row: 0, section: 0))
        
        XCTAssertEqual(receivedState, stateSent)
    }
    
    func test_didDeleteRow_removesItemFromCheckout() {
        
        let expectedCheckoutItem = CheckoutItem(product: TestUtils.someProductB, quantity: 2)
       
        let items = [CheckoutItem(product: TestUtils.someProductA, quantity: 2), expectedCheckoutItem]
        
        let checkout = Checkout(items: items)
        viewModel.checkout = checkout
        
        viewModel.didDeleteRow(at: IndexPath(row: 0, section: 0))
        
        XCTAssertTrue(viewModel.checkout.items.count == 1)
        XCTAssertEqual(viewModel.checkout.items[0], expectedCheckoutItem)
    }
    
    //MARK:- UIAdaptivePresentationControllerDelegate Methods
    //MARK: presentationControllerDidDismiss
    
    func test_presentationControllerDidDismiss_callsCloseView() {
        
        let mockCoordinator = MockCoordinator(navigationController: navigationController)
        viewModel.coordinator = mockCoordinator
        
        let presentationController = UIPresentationController(presentedViewController: UIViewController(), presenting: UIViewController())
        
        viewModel.presentationControllerDidDismiss(presentationController)
        
        let receivedCheckout = try? XCTUnwrap(mockCoordinator.closeBasketViewWithCheckoutWasCalledWith)
        XCTAssertEqual(receivedCheckout, checkout)
    }

}
