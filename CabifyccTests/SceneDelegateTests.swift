//
//  SceneDelegateTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import XCTest
@testable import Cabifycc

class SceneDelegateTests: XCTestCase {
    
    private var sceneDelegate: SceneDelegate!
    
    override func setUp() {
        super.setUp()
        
        guard let sceneDelegate = UIApplication.shared.connectedScenes.first?.delegate as? SceneDelegate else {
            XCTFail()
            return
        }
        self.sceneDelegate = sceneDelegate
    }
    
    override func tearDown() {
        sceneDelegate = nil
        super.tearDown()
    }

    func test_sceneDelegate_hasItsOwnRootCoordinator() {
        
        XCTAssertNotNil(sceneDelegate.coordinator)
    }
    
    func test_sceneDelegate_sceneWillConnectTo_coordinatorStarted() {
        
        let count = sceneDelegate.coordinator.navigationController.viewControllers.count
        XCTAssertTrue(count > 0)
    }
    
    func test_sceneDelegate_sceneWillConnectTo_setsCorrectWindowSceneSize() {
        
        let scene = UIApplication.shared.connectedScenes.first
        XCTAssertEqual((scene as? UIWindowScene)?.coordinateSpace.bounds, sceneDelegate.window?.bounds)
    }
}
