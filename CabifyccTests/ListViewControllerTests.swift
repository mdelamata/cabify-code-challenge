//
//  ListViewControllerTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import XCTest
@testable import Cabifycc

class ListViewControllerTests: XCTestCase {
    
    var viewModel: ListViewModel!
    var navigationController: UINavigationController!
    var listCoordinator: ListCoordinator!
    var listInteractor: ListInteractor! = ListInteractor()
    
    var listViewController: ListViewController!
    
    override func setUp() {
        super.setUp()
        
        navigationController = UINavigationController()
        listInteractor = ListInteractor()
        listCoordinator = ListCoordinator(navigationController: navigationController)
        viewModel = ListViewModel(coordinator: listCoordinator,
                                  interactor: listInteractor)
        
        listViewController = UIStoryboard.main.instantiateViewController(identifier: "ListViewController")
        listViewController.viewModel = viewModel
        
        _ = listViewController.view
    }
    
    override func tearDown() {
        listViewController = nil
        navigationController = nil
        listCoordinator = nil
        listInteractor = nil
        viewModel = nil
        
        super.tearDown()
    }
    
    //MARK:- viewDidLoad
    
    func test_viewDidLoad_callsConfigureUIandSetupsUI() {
        
        let mockViewModel = MockListViewModel()
        listViewController.viewModel = mockViewModel
        
        listViewController.viewDidLoad()
        
        XCTAssertTrue(mockViewModel.titleWasCalled)
        XCTAssertTrue(mockViewModel.viewBasketTitleWasCalled)
        let tableView = try? XCTUnwrap(mockViewModel.configureTableViewWasCalledWithTableView)
        XCTAssert(tableView == listViewController.tableView)
        XCTAssertNotNil(mockViewModel.renderAction)
        XCTAssertNotNil(listViewController.tableView.refreshControl)
    }
    
    
    func test_viewDidLoad_callsUpdateData() {
        
        let mockViewModel = MockListViewModel()
        listViewController.viewModel = mockViewModel
        
        listViewController.viewDidLoad()
        
        XCTAssertTrue(mockViewModel.fetchItemsWasCalled)
    }
    
    //MARK:- updateUI
    
    func test_updateUI_launch_tableviewIsAlphaZero() {
        
        listViewController.viewDidLoad()
        XCTAssertTrue(listViewController.tableView.alpha == 0)
    }
    
    //MARK:- updateData
    
    func test_updateData_callsFetchItems() {
        
        let mockViewModel = MockListViewModel()
        listViewController.viewModel = mockViewModel
        
        listViewController.updateData()
        
        XCTAssertTrue(mockViewModel.fetchItemsWasCalled)
    }
    
    //MARK:- refreshControl
    
    func test_refreshControl_updatesDataWhenExecutingAction() {
        
        let mockViewModel = MockListViewModel()
        listViewController.viewModel = mockViewModel
        
        listViewController.handleRefresh(listViewController.refreshControl)
        
        XCTAssertTrue(mockViewModel.fetchItemsWasCalled)
    }
    
    //MARK:- updateViewBasketView
    
    func test_updateViewBasketView_shouldShowViewBasket_updatesLabels() {
        
        let mockViewModel = MockListViewModel()
        listViewController.viewModel = mockViewModel
        
        mockViewModel.expectedShouldShowViewBasket = true
        
        let expectedProductsCount = "12"
        mockViewModel.expectedProductsCount = expectedProductsCount
        let expectedTotalPrice = "$12"
        mockViewModel.expectedTotalPrice = expectedTotalPrice
        
        listViewController.updateViewBasketView()
        
        XCTAssert(listViewController.productsCountLabel.text == expectedProductsCount)
        XCTAssert(listViewController.totalPriceLabel.text == expectedTotalPrice)
    }
    
    func test_updateViewBasketView_shouldNotShowViewBasket_doesNotUpdateLabels() {
        
        let mockViewModel = MockListViewModel()
        listViewController.viewModel = mockViewModel
        
        mockViewModel.expectedShouldShowViewBasket = false
        
        let expectedProductsCount = "12"
        mockViewModel.expectedProductsCount = expectedProductsCount
        let expectedTotalPrice = "$12"
        mockViewModel.expectedTotalPrice = expectedTotalPrice
        
        listViewController.updateViewBasketView()
        
        XCTAssertFalse(mockViewModel.productsCountWasCalled)
        XCTAssertFalse(mockViewModel.totalPriceWasCalled)
    }
    
    //MARK:- showActivityIndicator
    
    func test_showActivityIndicator_refreshControlRefreshing_keepsActivityIndicatorHidden() {
        listViewController.refreshControl.beginRefreshing()
        listViewController.showActivityIndicator()
        XCTAssertTrue(listViewController.activityIndicatorView.isHidden)
    }
    
    func test_showActivityIndicator_refreshControlNotRefreshing_showsActivityIndicator() {
        listViewController.refreshControl.endRefreshing()
        listViewController.showActivityIndicator()
        XCTAssertFalse(listViewController.activityIndicatorView.isHidden)
    }
    
    //MARK:- hideActivityIndicator
    
    func test_hideActivityIndicator_hidesActivityIndicator() {
        listViewController.hideActivityIndicator()
        XCTAssertTrue(listViewController.activityIndicatorView.isHidden)
        XCTAssertTrue(listViewController.refreshControl.isRefreshing == false)
    }
    
    //MARK:- viewBasketButtonPressed
    
    func test_viewBasketButtonPressed_callsViewBasketTapped() {
        
        let mockViewModel = MockListViewModel()
        listViewController.viewModel = mockViewModel
        
        listViewController.viewBasketButtonPressed(UIButton())
        
        XCTAssertTrue(mockViewModel.viewBasketTappedWasCalled)
    }
    
    
    //MARK:- RenderAction
    
    func test_renderAction_loadingState_showsActivityIndicator() {
        
        let mockActivityIndicator = MockActivityIndicator()
        listViewController.activityIndicatorView = mockActivityIndicator
        
        viewModel.renderAction?(.loading)
        
        XCTAssertTrue(mockActivityIndicator.isHiddenWasCalled)
        XCTAssertFalse(mockActivityIndicator.newIsHiddenValue)

    }
    
    func test_renderAction_failureState_showsActivityIndicator() {
        
        let mockActivityIndicator = MockActivityIndicator()
        listViewController.activityIndicatorView = mockActivityIndicator
        
        viewModel.renderAction?(.failure)
        
        XCTAssertTrue(mockActivityIndicator.isHiddenWasCalled)
        XCTAssertTrue(mockActivityIndicator.newIsHiddenValue)
    }
    
    func test_renderAction_updateState_callsRightMethods() {
        
        let mockActivityIndicator = MockActivityIndicator()
        listViewController.activityIndicatorView = mockActivityIndicator
        let mockTableView = MockTableView()
        listViewController.tableView = mockTableView
        
        let mockViewModel = MockListViewModel()
        mockViewModel.expectedShouldShowViewBasket = true
        listViewController.viewModel = mockViewModel
        
        viewModel.renderAction?(.update)
        
        XCTAssertTrue(mockActivityIndicator.isHiddenWasCalled)
        XCTAssertTrue(mockActivityIndicator.newIsHiddenValue)
        
        XCTAssertTrue(mockTableView.reloadDataWasCalled)
        
        XCTAssertTrue(mockViewModel.productsCountWasCalled)
        XCTAssertTrue(mockViewModel.totalPriceWasCalled)
    }

    //MARK:- Tableview hidden
    
    func test_renderAction_failureState_setsTableViewAlphaTo1() {
                
        viewModel.renderAction?(.failure)
        
        XCTAssertTrue(listViewController.tableView.alpha == 1)
    }
    
    func test_renderAction_updateState_setsTableViewAlphaTo1() {
                
        viewModel.renderAction?(.update)
        
        XCTAssertTrue(listViewController.tableView.alpha == 1)
    }
}

