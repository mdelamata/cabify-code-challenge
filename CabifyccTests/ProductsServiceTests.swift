//
//  ProductsServiceTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 29/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//


@testable import Cabifycc
import XCTest
import Combine

class ProductsServiceTests: XCTestCase {

    var productService: ProductsService! = ProductsService()
    
    override func tearDown() {
        productService = nil
        super.tearDown()
    }
    
    func test_fetchProducts_callsNetworkService() {
        
        let mockNetworkService = MockNetworkService<ProductList>()
        mockNetworkService.resultToReturn = .failure(NetworkError.requestError)
        
        productService.networkService = mockNetworkService
        
        _ = productService.fetchProducts()
        
        XCTAssertTrue(mockNetworkService.loadWasCalled)
    }
    
    func test_fetchPromotion_callsNetworkService() {
        
        let mockNetworkService = MockNetworkService<PromotionList>()
        mockNetworkService.resultToReturn = .failure(NetworkError.requestError)
        
        productService.networkService = mockNetworkService
        
        _ = productService.fetchPromotions()
        
        XCTAssertTrue(mockNetworkService.loadWasCalled)
    }

}
