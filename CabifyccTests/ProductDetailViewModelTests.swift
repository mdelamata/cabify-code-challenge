//
//  ProductDetailViewModelTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import XCTest
import Combine
import UIKit

class ProductDetailViewModelTests: XCTestCase {
    
    var viewModel: ProductDetailViewModel!
    var navigationController: UINavigationController!
    var listCoordinator: ListCoordinator!
    var product: Product!
    var quantity: Int!
    
    var quantitySubscriber: AnyCancellable!
    
    override func setUp() {
        super.setUp()
        navigationController = UINavigationController()
        listCoordinator = ListCoordinator(navigationController: navigationController)
        product = TestUtils.someProductA
        quantity = 10
        
        viewModel = ProductDetailViewModel(coordinator: listCoordinator,
                                           product: product,
                                           quantity: quantity)
    }
    
    override func tearDown() {
        navigationController = nil
        listCoordinator = nil
        viewModel = nil
        product = nil
        quantity = nil
        
        super.tearDown()
    }
    
    //MARK:- init
    
    func test_init_callsSubscribe() {
        XCTAssertNotNil(viewModel.subscriber)
    }
    
    //MARK:- Data
    
    func test_title_returnsProductName() {
        XCTAssertEqual(viewModel.title, product.name)
    }
    
    func test_price_returnsProductPriceFormatted() {
        XCTAssertEqual(viewModel.price, product.price.formattedString)
    }
    
    func test_description_productNoPromotion_returnsEmptyString() {
        XCTAssertEqual(viewModel.descriptionItem, "")
    }
    
    func test_description_productWithPromotion_returnsDescription() {
        
        product.updatePromotion(promotion: TestUtils.somePromotion)
        
        viewModel = ProductDetailViewModel(coordinator: listCoordinator,
                                           product: product,
                                           quantity: 10)
        
        XCTAssertEqual(viewModel.descriptionItem, product.promotion!.description)
    }
    
    func test_productQuantity_returnsPublisherLastValue() {
        
        let lastQuantity = 4
        
        viewModel.quantity.send(2)
        viewModel.quantity.send(3)
        viewModel.quantity.send(lastQuantity)
        
        XCTAssertEqual(viewModel.currentQuantity, "\(lastQuantity)")
    }
    
    //MARK: - Action Methods
    //MARK: increaseItemQuantity
    
    func test_increaseItemQuantity_updatesPublisherIncrementing() {
        
        let expectedValues: [Int] = [quantity, quantity + 1, quantity + 2, quantity + 3]
        var receivedValues: [Int] = []
        
        quantitySubscriber = viewModel.quantity
            .sink(receiveValue: { quantity in
                receivedValues.append(quantity)
            })
        
        for _ in 0..<3 {
            viewModel.increaseItemQuantity()
        }
        XCTAssertEqual(expectedValues, receivedValues)
    }
    
    func test_decreaseItemQuantity_updatesPublisherDecreasingWithLimitOnZero() {
        
        quantity = 3
        
        viewModel = ProductDetailViewModel(coordinator: listCoordinator,
                                           product: product,
                                           quantity: quantity)
        
        let expectedValues: [Int] = [quantity, 2, 1, 0]
        var receivedValues: [Int] = []
        
        quantitySubscriber = viewModel.quantity
            .sink(receiveValue: { quantity in
                receivedValues.append(quantity)
            })
        
        for _ in 0..<10 {
            viewModel.decreaseItemQuantity()
        }
        
        XCTAssertEqual(expectedValues, receivedValues)
    }
    
    //MARK: addItemsToBasket
    
    func test_addItemsToBasket_callsCoordinatorWithCurrentQuantityOnPublisher() {
        
        let mockCoordinator = MockCoordinator(navigationController: navigationController)
        
        viewModel = ProductDetailViewModel(coordinator: mockCoordinator,
                                           product: product,
                                           quantity: quantity)
        
        let lastValue = 15
        viewModel.quantity.send(lastValue)
        
        viewModel.addItemsToBasket()
        
        let receivedTuple = try? XCTUnwrap(mockCoordinator.dissmissDetailProductWithNewQuantityWasCalledWith)
        XCTAssertEqual(receivedTuple?.0, product)
        XCTAssertEqual(receivedTuple?.1, lastValue)
    }
    
    //MARK: dismiss
    
    func test_dismiss_callsCoordinatorWithQuantityNilOnPublisher() {
        
        let mockCoordinator = MockCoordinator(navigationController: navigationController)
        
        viewModel = ProductDetailViewModel(coordinator: mockCoordinator,
                                           product: product,
                                           quantity: quantity)
        
        let lastValue = 15
        viewModel.quantity.send(lastValue)
        
        viewModel.dismiss()
        
        let receivedTuple = try? XCTUnwrap(mockCoordinator.dissmissDetailProductWithNewQuantityWasCalledWith)
        XCTAssertEqual(receivedTuple?.0, product)
        XCTAssertNil(receivedTuple?.1)
    }
    
    //MARK: closeView
    
    func test_closeView_callsCoordinatorWithNewQuantityOnPublisher() {
        
        let mockCoordinator = MockCoordinator(navigationController: navigationController)
        
        viewModel = ProductDetailViewModel(coordinator: mockCoordinator,
                                           product: product,
                                           quantity: quantity)
        
        let sentValue = 15
        
        viewModel.closeView(withQuantity: sentValue)
        
        let receivedTuple = try? XCTUnwrap(mockCoordinator.dissmissDetailProductWithNewQuantityWasCalledWith)
        XCTAssertEqual(receivedTuple?.0, product)
        XCTAssertEqual(receivedTuple?.1, sentValue)
    }
    
}
