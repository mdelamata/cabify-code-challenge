//
//  ProductUtils.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import UIKit

struct TestUtils {
    static let someProductA = Product(code: "VOUCHER", name: "Cabify Voucher", price: 5)
    static let someProductB = Product(code: "TSHIRT", name: "Cabify T-Shirt", price: 20)
    static let someProductC = Product(code: "MUG", name: "Cabify Coffee Mug", price: 7.5)
    static let someProducts = [someProductA, someProductB, someProductC]
    static let someBasket = [someProductA: 1, someProductB: 3, someProductC: 10]
    static let somePromotion = Promotion(productCode: "VOUCHER", description: "", typeCode: .twoForOne)
    static let someProductList = ProductList(products: [someProductA])
    static let somePromotionList = PromotionList(promotions: [somePromotion])
    
    
    static let someProductCellViewModel = ProductCellViewModel(nameText: "name",
                                                               priceText: "price",
                                                               amountText: "amount",
                                                               image: UIImage(),
                                                               hasPromotion: false)
    
    static let someProductCellViewModelWithPromotion = ProductCellViewModel(nameText: "name",
                                                                            priceText: "price",
                                                                            amountText: "amount",
                                                                            image: UIImage(),
                                                                            hasPromotion: true)
    
    static let someCheckoutItemCellViewModel = CheckoutItemCellViewModel(nameText: "name",
                                                                         beforePromotionPriceText: "$10",
                                                                         afterPromotionPriceText: "$10",
                                                                         quantityText: "2x",
                                                                         image: UIImage())
}

extension TestUtils {
    
    static func setTestApplicationRootViewController(viewController: UIViewController) {
        let window = (UIApplication.shared.connectedScenes.first?.delegate as! SceneDelegate).window
        window?.rootViewController = viewController
    }
}
