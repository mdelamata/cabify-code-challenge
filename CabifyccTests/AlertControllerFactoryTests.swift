//
//  AlertControllerFactoryTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import XCTest
@testable import Cabifycc
import UIKit

class AlertControllerFactoryTests: XCTestCase {
    
    func test_createAlertController_returnsAlertControllerCorrectly() {
        
        let expectedTitle = "Title"
        let expectedMessage = "Message"
        
        
        let actions: [[String: ()->()]] = [Some.action1, Some.action2]
        
        let viewModel = AlertViewModel(title: expectedTitle,
                                       message: expectedMessage,
                                       actions: actions)
        
        let alertController = AlertControllerFactory.createAlertController(viewModel: viewModel)
        
        XCTAssertEqual(alertController.title, expectedTitle)
        XCTAssertEqual(alertController.message, expectedMessage)
        XCTAssertEqual(alertController.actions.count, 2)
    }
    
    func test_createAlertController_returnsAlertControllerWithDismiss() {
        
        let expectedTitle = "Title"
        let expectedMessage = "Message"
        let expectedDismissTitle = "Dismiss"
        
        let viewModel = AlertViewModel(title: expectedTitle,
                                       message: expectedMessage,
                                       dismiss:expectedDismissTitle)
        
        let alertController = AlertControllerFactory.createAlertController(viewModel: viewModel)
        
        XCTAssertEqual(alertController.title, expectedTitle)
        XCTAssertEqual(alertController.message, expectedMessage)
        XCTAssertEqual(alertController.actions.count, 1)
        XCTAssertEqual(alertController.actions.first?.title, expectedDismissTitle)
    }
    
    struct Some {
        static var action1: [String: ()->()] = ["action1": { print("action1") }]
        static var action2: [String: ()->()] = ["action2": { print("action2") }]
    }
}
