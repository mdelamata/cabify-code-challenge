//
//  CheckoutItemTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import XCTest
@testable import Cabifycc

class CheckoutItemTests: XCTestCase {
    
    //MARK: - beforePromotion
    
    func test_beforePromotion_returnsOriginalPrice() {
        
        let checkoutItemUT = checkoutItemPromo2x1(quantity: 2)
        
        let product = checkoutItemUT.product
        let originalPrice = product.price * Float(checkoutItemUT.quantity)
        
        XCTAssertEqual(checkoutItemUT.beforePromotionPrice, originalPrice)
    }
    
    //MARK: - afterPromotion
    
    func test_afterPromotion_noPromotion_returnsCorrectPrice() {
        
        let checkoutItemUT = checkoutItemNoPromotion()
        
        let originalPrice = checkoutItemUT.product.price * Float(checkoutItemUT.quantity)
        
        XCTAssertEqual(checkoutItemUT.afterPromotionPrice, originalPrice)
    }
    
    //MARK: - afterPromotion 2x1 promotion
    
    func test_afterPromotion_2x1_evenQuantity_returnsCorrectPrice() {
        
        let checkoutItemUT = checkoutItemPromo2x1(quantity: 2)
        
        let reducedPrice = checkoutItemUT.product.price
        
        XCTAssertEqual(checkoutItemUT.afterPromotionPrice, reducedPrice)
    }
    
    func test_afterPromotion_2x1_oddQuantity_returnsCorrectPrice() {
        
        let checkoutItemUT = checkoutItemPromo2x1(quantity: 3)
        
        let product = checkoutItemUT.product
        let reducedPrice = product.price + product.price
        
        XCTAssertEqual(checkoutItemUT.afterPromotionPrice, reducedPrice)
    }
    
    //MARK: - afterPromotion reduced price promotion
    
    func test_afterPromotion_reducedPrice_minimumQuantityNotMet_returnsCorrectPrice() {
        
        let checkoutItemUT = checkoutItemReducedPrice(quantity: 2,
                                                      minimumQuantity: 5,
                                                      reducedPrice: 1)
        
        let product = checkoutItemUT.product
        let nonReducedPrice = product.price * Float(checkoutItemUT.quantity)
        
        XCTAssertEqual(checkoutItemUT.afterPromotionPrice, nonReducedPrice)
    }
    
    func test_afterPromotion_reducedPrice_minimumQuantityMet_returnsCorrectPrice() {
        
        let potentialReducedPrice: Float = 2.0
        
        let checkoutItemUT = checkoutItemReducedPrice(quantity: 5,
                                                      minimumQuantity: 5,
                                                      reducedPrice: potentialReducedPrice)
        
        let reducedPrice = potentialReducedPrice * Float(checkoutItemUT.quantity)
        
        XCTAssertEqual(checkoutItemUT.afterPromotionPrice, reducedPrice)
    }
    
}

//MARK: - CheckoutItemTests Utils

extension CheckoutItemTests {
    
    func checkoutItemNoPromotion() -> CheckoutItem {
        CheckoutItem(product: TestUtils.someProductA, quantity: 10)
    }
    
    func checkoutItemPromo2x1(quantity: Int) -> CheckoutItem  {
        
        var productA = TestUtils.someProductA
        let promo2x1 = Promotion(productCode: productA.code,
                                 description: "",
                                 typeCode: .twoForOne)
        
        productA.updatePromotion(promotion: promo2x1)
        return CheckoutItem(product: productA, quantity: quantity)
    }
    
    func checkoutItemReducedPrice(quantity: Int, minimumQuantity: Int, reducedPrice: Float) -> CheckoutItem  {
        
        var productA = TestUtils.someProductA
        let promoReduced = Promotion(productCode: productA.code,
                                     description: "",
                                     minimumQuantity: minimumQuantity,
                                     reducedPrice: reducedPrice,
                                     typeCode: .reducedPrice)
        
        productA.updatePromotion(promotion: promoReduced)
        
        return CheckoutItem(product: productA, quantity: quantity)
    }
}
