//
//  BasketViewControllerTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import XCTest

class BasketViewControllerTests: XCTestCase {
    
    var viewModel: BasketViewModel!
    var navigationController: UINavigationController!
    var basketCoordinator: BasketCoordinating!
    var basketInteractor: BasketInteracting!
    var checkout: Checkout!
    
    var basketViewController: BasketViewController!
    
    override func setUp() {
        super.setUp()
        
        checkout = Checkout.empty()
        
        navigationController = UINavigationController()
        basketInteractor = BasketInteractor()
        basketCoordinator = BasketCoordinator(navigationController: navigationController,
                                              checkout: checkout)
        
        viewModel = BasketViewModel(coordinator: basketCoordinator,
                                    interactor: basketInteractor,
                                    checkout: checkout)
        
        guard
            let navController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "BasketViewControllerNavigationController") as? UINavigationController,
            let basketViewController = navController.viewControllers.first as? BasketViewController
            else {
                return
        }
        
        basketViewController.viewModel = viewModel
        _ = basketViewController.view
        
        self.basketViewController = basketViewController
    }
    
    override func tearDown() {
        _ = basketViewController.view
            = nil
        navigationController = nil
        basketCoordinator = nil
        basketInteractor = nil
        viewModel = nil
        checkout = nil
        
        super.tearDown()
    }
    
    //MARK:- viewDidLoad
    
    func test_viewDidLoad_configuresUI() {
        
        let mockViewModel = MockBasketViewModel()
        mockViewModel.stringToReturn = "test"
        basketViewController.viewModel = mockViewModel
        
        basketViewController.viewDidLoad()
        
        XCTAssertTrue(mockViewModel.titleWasCalled)
        XCTAssertEqual(mockViewModel.title(), basketViewController.title)
        
        XCTAssertNotNil(mockViewModel.renderAction)
        
        let tableView = try? XCTUnwrap(mockViewModel.configureTableViewWasCalledWithTableView)
        XCTAssert(tableView == basketViewController.tableView)
    }
    
    func test_viewDidLoad_setsViewModelAsAdaptativePresentationDelegate() {
        
        basketViewController.viewDidLoad()
        XCTAssert(basketViewController.navigationController?.presentationController?.delegate === viewModel)
    }
    
    func test_viewDidLoad_updatesUI() {
        
        let mockViewModel = MockBasketViewModel()
        mockViewModel.stringToReturn = "test"
        basketViewController.viewModel = mockViewModel
        
        let mockTableView = MockTableView()
        basketViewController.tableView = mockTableView

        basketViewController.viewDidLoad()
        
        XCTAssertTrue(mockViewModel.orderValueWasCalled)
        XCTAssertTrue(mockViewModel.orderValueAmountWasCalled)
        XCTAssertTrue(mockViewModel.savingsWasCalled)
        XCTAssertTrue(mockViewModel.savingsAmountWasCalled)
        XCTAssertTrue(mockViewModel.totalWasCalled)
        XCTAssertTrue(mockViewModel.totalAmountWasCalled)
        XCTAssertTrue(mockViewModel.payButtonTitleWasCalled)
        XCTAssertTrue(mockViewModel.payButtonEnabledWasCalled)
        XCTAssertTrue(mockTableView.reloadDataWasCalled)
    }
    
    //MARK: - updateUI

    func test_updateUI_expectedPayButtonEnabledFalse_payButtonAlphaIsChanged() {
        
        let mockViewModel = MockBasketViewModel()
        mockViewModel.expectedPayButtonEnabled = false
        
        basketViewController.viewModel = mockViewModel
        
        basketViewController.viewDidLoad()
        
        XCTAssert(basketViewController.payButton.alpha < 1)
    }
    
    func test_updateUI_expectedPayButtonEnabledTrue_payButtonAlphaOne() {
        
        let mockViewModel = MockBasketViewModel()
        mockViewModel.expectedPayButtonEnabled = true
        
        basketViewController.viewModel = mockViewModel
        
        basketViewController.viewDidLoad()
        
        XCTAssertEqual(basketViewController.payButton.alpha, 1)
    }
    
    //MARK: - cancelButtonPressed

    func test_cancelButtonPressed_callsCloseView() {

        let mockViewModel = MockBasketViewModel()
        basketViewController.viewModel = mockViewModel
        
        let buttonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: nil, action: nil)
        basketViewController.cancelButtonPressed(buttonItem)
        
        XCTAssertTrue(mockViewModel.closeViewWasCalled)
    }
    
    //MARK: - payButtonPressed

    func test_payButtonPressed_callsCloseView() {

        let mockViewModel = MockBasketViewModel()
        basketViewController.viewModel = mockViewModel
        
        basketViewController.payButtonPressed(UIButton())
        
        XCTAssertTrue(mockViewModel.payTappedWasCalled)
    }
}
