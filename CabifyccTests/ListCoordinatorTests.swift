//
//  ListCoordinatorTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import XCTest
@testable import Cabifycc

class ListCoordinatorTests: XCTestCase {
    
    var listCoordinator: ListCoordinator!
    var navigationController: UINavigationController!
    
    override func setUp() {
        super.setUp()
        self.navigationController = UINavigationController()
        self.listCoordinator = ListCoordinator(navigationController: navigationController)
    }
    
    override func tearDown() {
        listCoordinator = nil
        navigationController = nil
        super.tearDown()
    }
    
    // MARK: - start
    
    func test_start_initialiazesListViewController() {
        
        listCoordinator.start()
        
        let viewController = navigationController.viewControllers.first
        
        XCTAssert(viewController is ListViewController)
        XCTAssert(navigationController.viewControllers.count == 1)
        XCTAssertNotNil((viewController as? ListViewController)?.viewModel)
    }

    func test_start_assignsViewModelToViewController() {
        
        listCoordinator.start()
        
        let listViewController = navigationController.viewControllers.first as! ListViewController
        let viewModel = listViewController.viewModel
        
        XCTAssertNotNil(viewModel)
        XCTAssert((viewModel as? ListViewModel)?.coordinator === listCoordinator)
    }
    
    //MARK: - finish()
    
    func test_finish_parentCallDidFinishCorrectly() {
        
        let mockCoordinator = MockCoordinator(navigationController: navigationController)
        
        listCoordinator.parentCoordinator = mockCoordinator
        
        listCoordinator.finish()
        
        XCTAssertNotNil(mockCoordinator.childDidFinishWasCalledWithCoordinator)
        let coordinator = try? XCTUnwrap(mockCoordinator.childDidFinishWasCalledWithCoordinator)
        XCTAssert(coordinator === listCoordinator)
    }
    
    //MARK:- ListCoordinating Methods

    //MARK: showBasketView

    func test_showBasketView_callsParentWithCheckout() {
        
        let mockCoordinator = MockCoordinator(navigationController: navigationController)
        listCoordinator.parentCoordinator = mockCoordinator
        
        listCoordinator.showBasketView(checkout: Checkout.empty())
        
        XCTAssertNotNil(mockCoordinator.createBasketViewControllerWasCalledWithCheckout)
    }
    
    func test_showDetail_createsProductDetailViewControllerCorrectly() {
        
        let product = TestUtils.someProductA
        let quantity = 10

        TestUtils.setTestApplicationRootViewController(viewController: navigationController)
        
        listCoordinator.showDetail(product: product, currentQuantity: quantity)
        
        let productDetailViewController = try? XCTUnwrap(navigationController.presentedViewController as? ProductDetailViewController)
        let productDetailViewModel = try? XCTUnwrap((productDetailViewController?.viewModel as? ProductDetailViewModel))

        XCTAssert(productDetailViewModel?.coordinator === listCoordinator)
        XCTAssert(productDetailViewModel?.product == product)
        XCTAssert(productDetailViewModel?.quantity.value == quantity)
    }
    
    //MARK: dissmissDetail

    func test_dismissDetail_dismissesView() {
        
        let product = TestUtils.someProductA
        let quantity = 10
        
        let mockNavigationController = MockNavigationController()
        mockNavigationController.presentedViewControllerToReturn = UIViewController()
        listCoordinator.navigationController = mockNavigationController

        listCoordinator.start()
        
        let mockListViewModel = MockListViewModel()
        listCoordinator.listViewController.viewModel = mockListViewModel

        listCoordinator.dismissDetail(product: product, newQuantity: quantity)

        XCTAssertTrue(mockNavigationController.dismissWasCalled)
        let receivedTuple = try? XCTUnwrap(mockListViewModel.setBasketWithQuantityWasCalledWith)
        XCTAssertEqual(product, receivedTuple?.0)
        XCTAssertEqual(quantity, receivedTuple?.1)
    }
    
    //MARK: updateCheckout
    
    func test_updateCheckout_updatesBasketViewModel() {
        
        let mockListViewModel = MockListViewModel()
        
        listCoordinator.start()
        listCoordinator.listViewController.viewModel = mockListViewModel
        
        listCoordinator.updateCheckout(Checkout.empty())
        
        XCTAssertNotNil(mockListViewModel.updateBasketWasCalledWithBasket)
    }

}
