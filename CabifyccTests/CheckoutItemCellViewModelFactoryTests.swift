//
//  CheckoutItemCellViewModelFactoryTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 29/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import XCTest
import UIKit

class CheckoutItemCellViewModelFactoryTests: XCTestCase {
    
    var factory: CheckoutItemCellViewModelFactory! = CheckoutItemCellViewModelFactory()
    
    override func tearDown() {
        factory = nil
        super.tearDown()
    }
    
    func test_createCheckoutItemCellViewModel_checkoutItemNoPromotion_returnsCorrectViewModel() {
        
        let product = TestUtils.someProductA
        let quantity = 2
        
        let checkoutItem = CheckoutItem(product: product, quantity: quantity)
        
        let viewModel = factory.createCheckoutItemCellViewModel(for: checkoutItem)
        
        XCTAssert(viewModel.nameText == "x2 \(product.name)")
        XCTAssert(viewModel.quantityText == "2")
        XCTAssert(viewModel.beforePromotionPriceText == "")
        XCTAssert(viewModel.afterPromotionPriceText == "$10.00")
        XCTAssertNotNil(viewModel.image)
    }
    
    func test_createCheckoutItemCellViewModel_checkoutItemWithPromotion_returnsCorrectViewModel() {
        
        var product = TestUtils.someProductA
        let quantity = 2
        
        product.updatePromotion(promotion: TestUtils.somePromotion)
        
        let checkoutItem = CheckoutItem(product: product, quantity: quantity)
        
        let viewModel = factory.createCheckoutItemCellViewModel(for: checkoutItem)
        
        XCTAssert(viewModel.nameText == "x2 \(product.name)")
        XCTAssert(viewModel.quantityText == "2")
        XCTAssert(viewModel.beforePromotionPriceText == "$10.00")
        XCTAssert(viewModel.afterPromotionPriceText == "$5.00")
        XCTAssertNotNil(viewModel.image)
    }
    
    func test_createCheckoutItemCellViewModel_voucherProduct_imageCorrect() {
        
        let productVoucher = Product(code: "VOUCHER", name: "", price: 1.0, promotion: nil)
        let checkoutItem = CheckoutItem(product: productVoucher, quantity: 1)

        let viewModel = factory.createCheckoutItemCellViewModel(for: checkoutItem)
        
        XCTAssertEqual(viewModel.image, UIImage(named: "voucher"))
    }
    
    func test_createCheckoutItemCellViewModel_tshirtProduct_imageCorrect() {
        
        let productTshirt = Product(code: "TSHIRT", name: "", price: 1.0, promotion: nil)
        let checkoutItem = CheckoutItem(product: productTshirt, quantity: 1)

        let viewModel = factory.createCheckoutItemCellViewModel(for: checkoutItem)
        
        XCTAssertEqual(viewModel.image, UIImage(named: "tshirt"))
    }
    
    func test_createCheckoutItemCellViewModel_mugProduct_imageCorrect() {
        
        let productMug = Product(code: "MUG", name: "", price: 1.0, promotion: nil)
        let checkoutItem = CheckoutItem(product: productMug, quantity: 1)

        let viewModel = factory.createCheckoutItemCellViewModel(for: checkoutItem)
        
        XCTAssertEqual(viewModel.image, UIImage(named: "mug"))
    }
    
    func test_createCheckoutItemCellViewModel_uknownProduct_imageCorrect() {
        
        let productUnknown = Product(code: "BOOK", name: "", price: 1.0, promotion: nil)
        let checkoutItem = CheckoutItem(product: productUnknown, quantity: 1)

        let viewModel = factory.createCheckoutItemCellViewModel(for: checkoutItem)
        
        XCTAssertEqual(viewModel.image, UIImage(named: "unknown"))
    }
}
