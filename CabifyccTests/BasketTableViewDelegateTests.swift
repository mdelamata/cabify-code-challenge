//
//  BasketTableViewDelegateTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 29/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import XCTest
import UIKit

class BasketTableViewDelegateTests: XCTestCase {
    
    var mockViewModel: MockBasketViewModel!
    var basketTableViewDelegate: BasketTableViewDelegate!
    
    override func setUp() {
        super.setUp()
        
        mockViewModel = MockBasketViewModel()
        basketTableViewDelegate = BasketTableViewDelegate(viewModel: mockViewModel)
    }
    
    override func tearDown() {
        mockViewModel = nil
        basketTableViewDelegate = nil
        super.tearDown()
    }
    
    //MARK:- UITableViewDelegate
    
    func test_tableViewDidSelectRowAtIndexPath_forwardsCallToViewModel() {
        
        let mockTableView = MockTableView()
        let expectedIndexPath = IndexPath(row: 0, section: 0)
        
        basketTableViewDelegate.tableView(mockTableView, didSelectRowAt: expectedIndexPath)
        
        let receivedIndexPathFromViewModel = try? XCTUnwrap(mockViewModel.didSelectRowAtIndexPathWasCalledWith)
        XCTAssertEqual(expectedIndexPath, receivedIndexPathFromViewModel)
    }
    
}
