//
//  ResourceTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 29/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import XCTest

class ResourceTests: XCTestCase {
    
    func test_resource_requestBuiltCorrectly() {
        
        let resource = Resource<ProductList>(url: URL(string: "https://www.google.com")!)
        XCTAssertNotNil(resource.request)
    }
    
    func test_resource_withParams_requestBuiltCorrectly() {
        
        let url = URL(string: "https://www.google.com")!
        let resourceWithParameters = Resource<ProductList>(url: url,
                                                           parameters: ["test":"testValue"])
        let request = resourceWithParameters.request
        XCTAssertNotNil(request)
    }
    
    //MARK:- Resource+ProductList
    
    func test_products_returnsCorrectResource() {
        XCTAssertNotNil(Resource<ProductList>.products().request)
    }
    
    //MARK:- Resource+PromotionList
    
    func test_promotions_returnsCorrectResource() {
        XCTAssertNotNil(Resource<PromotionList>.promotions().request)
    }
}
