//
//  BasketTableViewDataSourceTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import XCTest
import UIKit

class BasketTableViewDataSourceTests: XCTestCase {
    
    var mockViewModel: MockBasketViewModel!
    var basketTableViewDataSource: BasketTableViewDataSource!
    
    override func setUp() {
        super.setUp()
        
        mockViewModel = MockBasketViewModel()
        basketTableViewDataSource = BasketTableViewDataSource(viewModel: mockViewModel)
    }
    
    override func tearDown() {
        mockViewModel = nil
        basketTableViewDataSource = nil
        super.tearDown()
    }
    
    //MARK:- UITableViewDataSource
    
    func test_tableViewNumberOfRowsInSection_callsViewModelNumberOfItems() {
        
        _ = basketTableViewDataSource.tableView(UITableView(), numberOfRowsInSection: 0)
        XCTAssertTrue(mockViewModel.numberOfItemsWasCalled)
    }
    
    func test_tableViewCellForRowAtIndexPath_callsViewModelGetCellViewModel() {
        
        let expectedIndexPath = IndexPath(row: 0, section: 0)
        mockViewModel.expectedCheckoutItemCellViewModelToReturn = TestUtils.someCheckoutItemCellViewModel
        
        let mockProductTableViewCell = MockCheckoutItemTableViewCell()
        
        let mockTableView = MockTableView()
        mockTableView.dequeuedCellToReturn = mockProductTableViewCell
        
        _ = basketTableViewDataSource.tableView(mockTableView, cellForRowAt: expectedIndexPath)
        
        let receivedIndexPath = try? XCTUnwrap(mockViewModel.getCellViewModelAtIndexPathWasCalledWith)
        XCTAssertEqual(receivedIndexPath, expectedIndexPath)
        
        XCTAssertTrue(mockProductTableViewCell.configureWasCalled)
    }
    
    func test_tableViewDeleteRowAtIndexPath_callsViewModelGetCellViewModel() {
        
        let expectedIndexPath = IndexPath(row: 0, section: 0)
        
        let mockTableView = MockTableView()
        
        _ = basketTableViewDataSource.tableView(mockTableView, commit: .delete, forRowAt: expectedIndexPath)
        
        let receivedIndexPath = try? XCTUnwrap(mockViewModel.didDeleteRowAtIndexPathWasCalledWith)
        XCTAssertEqual(receivedIndexPath, expectedIndexPath)
    }
}
