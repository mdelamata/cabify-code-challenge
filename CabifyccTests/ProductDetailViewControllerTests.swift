//
//  ProductDetailViewControllerTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//


@testable import Cabifycc
import UIKit
import XCTest

class ProductDetailViewControllerTests: XCTestCase {
    
    var viewModel: ProductDetailViewModel!
    var navigationController: UINavigationController!
    var listCoordinator: ListCoordinator!
    var productDetailViewController: ProductDetailViewController!
    
    var product: Product!
    var currentQuantity: Int!
    
    override func setUp() {
        super.setUp()
        
        navigationController = UINavigationController()
        listCoordinator = ListCoordinator(navigationController: navigationController)
        
        product = TestUtils.someProductA
        currentQuantity = 10
        
        viewModel = ProductDetailViewModel(coordinator: listCoordinator,
                                           product: product,
                                           quantity: currentQuantity)
        
        productDetailViewController = UIStoryboard.main.instantiateViewController(identifier: "ProductDetailViewController")
        productDetailViewController.viewModel = viewModel
        
        _ = productDetailViewController.view
    }
    
    override func tearDown() {
        productDetailViewController = nil
        navigationController = nil
        listCoordinator = nil
        viewModel = nil
        
        super.tearDown()
    }
    
    //MARK:- viewDidLoad
    
    func test_viewDidLoad_setsViewModelAsAdaptativePresentationDelegate() {
        
        productDetailViewController.viewDidLoad()
        XCTAssert(productDetailViewController.presentationController?.delegate === viewModel)
    }
    
    func test_viewDidLoad_configuresRenderAction() {
        
        productDetailViewController.viewDidLoad()
        XCTAssertNotNil(viewModel.renderAction)
    }
    
    func test_viewDidLoad_callsConfigureUI() {
        
        productDetailViewController.viewDidLoad()

        XCTAssertTrue(productDetailViewController.titleLabel.text == viewModel.title)
        XCTAssertTrue(productDetailViewController.descriptionLabel.text == viewModel.descriptionItem)
        XCTAssertTrue(productDetailViewController.priceLabel.text == viewModel.price)
        XCTAssertTrue(productDetailViewController.amountLabel.text == viewModel.currentQuantity)
        XCTAssertTrue(productDetailViewController.chooseAmountLabel.text == viewModel.chooseQuantity)
        XCTAssertTrue(productDetailViewController.addToBasketButton.titleLabel?.text == viewModel.addToBasket)
    }
    
    //MARK: - IBAction Methods
    
    //MARK: - decreaseButtonPressed
    
    func test_decreaseButtonPressed_callsViewModelDecreaseItemQuantity() {
        
        let mockViewModel = MockProductDetailViewModel()
        productDetailViewController.viewModel = mockViewModel
        
        productDetailViewController.decreaseButtonPressed(UIButton())
        
        XCTAssertTrue(mockViewModel.decreaseItemQuantityWasCalled)
    }
    
    func test_decreaseButtonPressed_callsFeedbackGenerator() {
        
        let mockFeedbackGenerator = MockFeedbackGenerator()
        productDetailViewController.feedbackGenerator = mockFeedbackGenerator
        
        productDetailViewController.decreaseButtonPressed(UIButton())

        XCTAssertTrue(mockFeedbackGenerator.selectionDidChangeWasCalled)
    }
    
    //MARK: - increaseButtonPressed
    
    func test_increaseButtonPressed_callsViewModelIncreaseItemQuantity() {
        
        let mockViewModel = MockProductDetailViewModel()
        productDetailViewController.viewModel = mockViewModel
        
        productDetailViewController.increaseButtonPressed(UIButton())
        
        XCTAssertTrue(mockViewModel.increaseItemQuantityWasCalled)
    }
    
    func test_increaseButtonPressed_callsFeedbackGenerator() {
        
        let mockFeedbackGenerator = MockFeedbackGenerator()
        productDetailViewController.feedbackGenerator = mockFeedbackGenerator
        
        productDetailViewController.increaseButtonPressed(UIButton())

        XCTAssertTrue(mockFeedbackGenerator.selectionDidChangeWasCalled)
    }
    
    //MARK: - addToBaketButtonPressed

    func test_addToBaketButtonPressed_callsViewModelAddItemsToBaket() {
        
        let mockViewModel = MockProductDetailViewModel()
        productDetailViewController.viewModel = mockViewModel
        
        productDetailViewController.addToBaketButtonPressed(UIButton())
        
        XCTAssertTrue(mockViewModel.addItemsToBasketWasCalled)
    }
    
    func test_addToBaketButtonPressed_callsFeedbackGenerator() {
        
        let mockFeedbackGenerator = MockFeedbackGenerator()
        productDetailViewController.feedbackGenerator = mockFeedbackGenerator
        
        productDetailViewController.addToBaketButtonPressed(UIButton())
        
        XCTAssertTrue(mockFeedbackGenerator.rigidImpactOccurredWasCalled)
    }
    
    //MARK: - closeButtonPressed

    func test_closeButtonPressed_callsViewModelDismiss() {
        
        let mockViewModel = MockProductDetailViewModel()
        productDetailViewController.viewModel = mockViewModel
        
        productDetailViewController.closeButtonPressed(UIButton())
        
        XCTAssertTrue(mockViewModel.dismissWasCalled)
    }
    
    //MARK: - Render Action
    
    func test_renderAction_respondsToState() {
        
        productDetailViewController.amountLabel.text = ""
        
        viewModel.renderAction?(.updateQuantity)
        
        XCTAssert(productDetailViewController.amountLabel.text == viewModel.currentQuantity)
    }
}
