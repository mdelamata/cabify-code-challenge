//
//  RootCoordinatorTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import XCTest
@testable import Cabifycc

class RootCoordinatorTests: XCTestCase {
    
    var rootCoordinator: RootCoordinator!
    var navigationController: UINavigationController!
    
    override func setUp() {
        super.setUp()
        self.navigationController = UINavigationController()
        self.rootCoordinator = RootCoordinator(navigationController: navigationController)
    }
    
    override func tearDown() {
        rootCoordinator = nil
        super.tearDown()
    }
    
    // MARK: - start
    
    func test_start_initialiazesListCoordinator() {
        
        XCTAssert(rootCoordinator.childCoordinators.count == 0)
        
        rootCoordinator.start()
        
        let listCoordinator = rootCoordinator.childCoordinators.first!
        
        XCTAssert(listCoordinator is ListCoordinating)
        XCTAssert(rootCoordinator.childCoordinators.count == 1)
    }
    
    // MARK: - createListViewController
    
    func test_createListViewController_callsListCoordinatorStart() {
        
        let mockCoordinator = MockCoordinator(navigationController: navigationController)
        
        var mockCoordinatorFactory = MockCoordinatorFactory()
        mockCoordinatorFactory.coordinatorToReturn = mockCoordinator

        rootCoordinator.coordinatorFactory = mockCoordinatorFactory
        
        rootCoordinator.createListViewController()
        
        XCTAssertTrue(mockCoordinator.startWasCalled)
    }
    
    func test_createListViewController_setsParentCoordinator() {
        
        rootCoordinator.createListViewController()
        XCTAssertNotNil(rootCoordinator.childCoordinators.first!.parentCoordinator)
    }
    
    // MARK: - createBasketViewController

    func test_createBasketViewController_callsBasketCoordinatorStart() {
        
        let mockCoordinator = MockCoordinator(navigationController: navigationController)
        
        var mockCoordinatorFactory = MockCoordinatorFactory()
        mockCoordinatorFactory.coordinatorToReturn = mockCoordinator

        rootCoordinator.coordinatorFactory = mockCoordinatorFactory
        
        rootCoordinator.createBasketViewController(checkout: Checkout.empty())
        
        XCTAssertTrue(mockCoordinator.startWasCalled)
    }
    
    func test_createBasketViewController_setsParentCoordinator() {
        
        rootCoordinator.createBasketViewController(checkout: Checkout.empty())
        XCTAssertNotNil(rootCoordinator.childCoordinators.first!.parentCoordinator)
    }
    
    //MARK:- childDidFinish
    
    func test_childDidFinish_removesCoordinatorCorrectly() {
        
        let coordinatorFactory = CoordinatorFactory()
        
        let coordinator1 = coordinatorFactory.createListCoordinator(navigationController: navigationController)
        let coordinator2 = coordinatorFactory.createListCoordinator(navigationController: navigationController)

        rootCoordinator.childCoordinators.append(contentsOf: [coordinator1, coordinator2])
        
        rootCoordinator.childDidFinish(coordinator1)
        
        XCTAssertFalse(rootCoordinator.childCoordinators.contains(where: { $0 === coordinator1 }))
        XCTAssertTrue(rootCoordinator.childCoordinators.count == 1)
        
        rootCoordinator.childDidFinish(coordinator2)

        XCTAssertFalse(rootCoordinator.childCoordinators.contains(where: { $0 === coordinator2 }))
        XCTAssertTrue(rootCoordinator.childCoordinators.isEmpty)
    }
}
