//
//  BasketCoordinatorTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import UIKit
import XCTest

class BasketCoordinatorTests: XCTestCase {
    
    var basketCoordinator: BasketCoordinator!
    var navigationController: UINavigationController!
    var checkout: Checkout!
    
    override func setUp() {
        super.setUp()
        
        navigationController = UINavigationController()
        checkout = Checkout.empty()
        basketCoordinator = BasketCoordinator(navigationController: navigationController,
                                              checkout: checkout)
    }
    
    override func tearDown() {
        checkout = nil
        basketCoordinator = nil
        navigationController = nil
        super.tearDown()
    }
    
    // MARK: - start
    
    func test_start_initialiazesBasketViewController() {
        
        TestUtils.setTestApplicationRootViewController(viewController: navigationController)
        
        basketCoordinator.start()
        
        let viewController = navigationController.presentedViewController
        
        XCTAssert(viewController is UINavigationController)
        
        guard let navController = viewController as? UINavigationController,
            let basketViewController = navController.viewControllers.first
            else {
                XCTFail()
                return
        }
        XCTAssert(basketViewController is BasketViewController)
        
        XCTAssertNotNil((basketViewController as? BasketViewController)?.viewModel)
    }
    
    // MARK: - finish
    
    func test_finish_callsParentCoordinatorChildDidFinish() {
        
        let mockCoordinator = MockCoordinator(navigationController: navigationController)
        basketCoordinator.parentCoordinator = mockCoordinator
        
        basketCoordinator.finish()
        
        let coordinator = try? XCTUnwrap(mockCoordinator.childDidFinishWasCalledWithCoordinator)
        XCTAssert(coordinator === basketCoordinator)
    }
    
    //MARK:- BasketCoordinating Methods
    
    //MARK:- closeBasket
    
    func test_closeBasket_presentedViewController_callsNavigationControllerDismiss() {
        
        let mockNavigationController = MockNavigationController()
        mockNavigationController.presentedViewControllerToReturn = UIViewController()
        basketCoordinator.navigationController = mockNavigationController
        
        basketCoordinator.closeBasket(checkout: Checkout.empty())
        
        XCTAssertTrue(mockNavigationController.dismissWasCalled)
    }
    
    func test_closeBasket_noPresentedViewController_doesNotcallNavigationControllerDismiss() {
        
        let mockNavigationController = MockNavigationController()
        mockNavigationController.presentedViewControllerToReturn = nil
        basketCoordinator.navigationController = mockNavigationController
        
        basketCoordinator.closeBasket(checkout: Checkout.empty())
        
        XCTAssertFalse(mockNavigationController.dismissWasCalled)
    }
    
    func test_closeBasket_callsUpdateCheckout() {
        
        let mockParentCoordinator = MockCoordinator(navigationController: navigationController)
        basketCoordinator.parentCoordinator = mockParentCoordinator
        
        let checkout = Checkout.empty()
        basketCoordinator.closeBasket(checkout: checkout)
        
        let receivedCheckout = try? XCTUnwrap(mockParentCoordinator.updateCheckoutWasCalledWithCheckout)
        XCTAssert(receivedCheckout == checkout)
    }
    
    func test_closeBasket_callsFinish() {
        
        let mockCoordinator = MockCoordinator(navigationController: navigationController)
        basketCoordinator.parentCoordinator = mockCoordinator
        
        let checkout = Checkout.empty()
        basketCoordinator.closeBasket(checkout: checkout)
        
        let coordinator = try? XCTUnwrap(mockCoordinator.childDidFinishWasCalledWithCoordinator)
        XCTAssert(coordinator === basketCoordinator)
    }
}
