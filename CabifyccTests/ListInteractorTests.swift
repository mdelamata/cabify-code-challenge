//
//  ListInteractorTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import XCTest
@testable import Cabifycc
import Combine

class ListInteractorTests: XCTestCase {
    
    var listInteractor: ListInteractor! = ListInteractor()
    var interactorStateSubscriber: AnyCancellable!
    var productsSubscriber: AnyCancellable!
    var checkoutSubscriber: AnyCancellable!
    
    override func tearDown() {
        listInteractor = nil
        interactorStateSubscriber = nil
        productsSubscriber = nil
        checkoutSubscriber = nil
        super.tearDown()
    }
    
    // MARK: - fetchItems
    
    func test_fetchItems_callsBothProductsAndPromotionsFetches() {
        
        let mockProductsService = MockProductsService()
        mockProductsService.fecthProductsResultToReturn = .success(TestUtils.someProductList)
        mockProductsService.fetchPromotionsResultToReturn = .success(TestUtils.somePromotionList)
        
        listInteractor.productsService = mockProductsService
        
        listInteractor.fetchItems()
        
        XCTAssertTrue(mockProductsService.fecthProductsWasCalled)
        XCTAssertTrue(mockProductsService.fetchPromotionsWasCalled)
    }
    
    func test_fetchItems_successfully_changesProductsStateCorrectly() {
        
        let expectedProducts = TestUtils.someProductList.products
        let expectedProductsStates: [[Product]] = [[], TestUtils.someProductList.products]
        var receivedProductsUpdates: [[Product]] = []
        
        let mockProductsService = MockProductsService()
        mockProductsService.fecthProductsResultToReturn = .success(ProductList(products: expectedProducts))
        mockProductsService.fetchPromotionsResultToReturn = .success(PromotionList(promotions: []))
        
        listInteractor.productsService = mockProductsService
        
        productsSubscriber = listInteractor.products
            .sink { products in
                receivedProductsUpdates.append(products)
        }
        
        listInteractor.fetchItems()
        
        XCTAssertEqual(receivedProductsUpdates, expectedProductsStates)
    }
    
    func test_fetchItems_failure_doesNotOverridePreviousSuccessfulCall() {
        
        let expectedProducts = TestUtils.someProductList.products
        let expectedProductsStates: [[Product]] = [[], TestUtils.someProductList.products]
        var receivedProductsUpdates: [[Product]] = []
        
        let mockProductsService = MockProductsService()
        mockProductsService.fecthProductsResultToReturn = .success(ProductList(products: expectedProducts))
        mockProductsService.fetchPromotionsResultToReturn = .success(PromotionList(promotions: []))
        
        listInteractor.productsService = mockProductsService
        
        productsSubscriber = listInteractor.products
            .sink { products in
                receivedProductsUpdates.append(products)
        }
        
        listInteractor.fetchItems()
        
        mockProductsService.fecthProductsResultToReturn = .failure(NetworkError.noInternetError)
        
        listInteractor.fetchItems()
        
        XCTAssertEqual(receivedProductsUpdates, expectedProductsStates)
    }
    
    func test_fetchItems_successfully_changesListInteractorStateCorrectly() {
        
        var receivedStates = [ListInteractorState]()
        let expectedStates: [ListInteractorState] = [.idle, .loading, .success, .idle]
        
        let mockProductsService = MockProductsService()
        mockProductsService.fecthProductsResultToReturn = .success(TestUtils.someProductList)
        mockProductsService.fetchPromotionsResultToReturn = .success(TestUtils.somePromotionList)
        
        listInteractor.productsService = mockProductsService
        
        interactorStateSubscriber = listInteractor.state
            .sink { state in
                receivedStates.append(state)
        }
        
        listInteractor.fetchItems()
        
        XCTAssertEqual(receivedStates, expectedStates)
    }
    
    func test_fetchItems_failing_changesListInteractorStateCorrectly() {
        
        let expectedError = NetworkError.noInternetError
        
        var states = [ListInteractorState]()
        let expectedStates: [ListInteractorState] = [.idle, .loading, .failure(expectedError), .idle]
        
        let mockProductsService = MockProductsService()
        mockProductsService.fecthProductsResultToReturn = .success(TestUtils.someProductList)
        mockProductsService.fetchPromotionsResultToReturn = .failure(expectedError)
        
        listInteractor.productsService = mockProductsService
        
        interactorStateSubscriber = listInteractor.state
            .sink { state in
                states.append(state)
        }
        
        listInteractor.fetchItems()
        
        XCTAssertEqual(states, expectedStates)
    }
    
    // MARK: - updateBasket
    
    func test_updateBasket_changesBasket() {
        
        var newBasket = Basket()
        newBasket[TestUtils.someProductA] = 2
        
        listInteractor.updateBasket(basket: newBasket)
        
        XCTAssertEqual(listInteractor.basket, newBasket)
    }
    
    func test_updateBasket_updatesCheckout() {
        
        var newBasket = Basket()
        newBasket[TestUtils.someProductA] = 2
        
        var receivedCheckoutStates: [Checkout] = []
        let expectedCheckoutStates: [Checkout] = [Checkout.empty(), newBasket.toCheckout]
        
        checkoutSubscriber = listInteractor.checkout
            .sink { checkout in
                receivedCheckoutStates.append(checkout)
        }
        
        listInteractor.updateBasket(basket: newBasket)
        
        XCTAssertEqual(receivedCheckoutStates, expectedCheckoutStates)
    }
    
    // MARK: - setBasketProduct:WithQuantity:
    
    func test_setBasketProductWithQuantity_updatesBasketCorrectly() {
        
        let product = TestUtils.someProductA
        let quantity = 2
        
        listInteractor.setBasket(product: product, withQuantity: quantity)
        
        XCTAssertEqual(listInteractor.basket[product], quantity)
    }
    
    func test_setBasketProductWithQuantity_updatesCheckout() {
        
        let product = TestUtils.someProductA
        let quantity = 2
        
        let expectedUpdatedCheckout = [product: quantity].toCheckout
        
        var receivedCheckoutStates: [Checkout] = []
        let expectedCheckoutStates: [Checkout] = [Checkout.empty(), expectedUpdatedCheckout]
        
        checkoutSubscriber = listInteractor.checkout
            .sink { checkout in
                receivedCheckoutStates.append(checkout)
        }
        
        listInteractor.setBasket(product: product, withQuantity: quantity)
        
        XCTAssertEqual(receivedCheckoutStates, expectedCheckoutStates)
    }
    
    // MARK: - basketQuantity
    
    func test_basketQuantity_returnsCorrectValues() {
        
        let basket = [TestUtils.someProductA: 2]
        listInteractor.basket = basket
        
        XCTAssertEqual(listInteractor.basketQuantity(for: TestUtils.someProductA), 2)
        XCTAssertEqual(listInteractor.basketQuantity(for: TestUtils.someProductB), 0)
    }
    
    // MARK: - updateCheckout
    
    func test_updateCheckout_updatesCheckoutSubject() {
        
        var receivedCheckout: Checkout?
                
        checkoutSubscriber = listInteractor.checkout
            .sink { checkout in
                receivedCheckout = checkout
        }
        
        var newBasket = Basket()
        newBasket[TestUtils.someProductA] = 2
             
        listInteractor.updateCheckout(basket: newBasket)
        
        XCTAssertEqual(newBasket.toCheckout, receivedCheckout)
    }
}
