//
//  ProductCellViewModelFactoryTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import XCTest
import UIKit

class ProductCellViewModelFactoryTests: XCTestCase {
    
    var factory: ProductCellViewModelFactory! = ProductCellViewModelFactory()
    
    override func tearDown() {
        factory = nil
        super.tearDown()
    }
    
    func test_createProductCellViewModel_productWithTwoItemsOnBasket_returnsCorrectViewModel() {
        
        let product = TestUtils.someProductA
        let quantity = 2
        
        let viewModel = factory.createProductCellViewModel(for: product, amount: quantity)
        
        XCTAssert(viewModel.nameText == product.name)
        XCTAssert(viewModel.amountText == "x2")
        XCTAssert(viewModel.priceText == "$5.00")
        XCTAssert(viewModel.hasPromotion == false)
        XCTAssertNotNil(viewModel.image)
    }
    
    func test_createProductCellViewModel_productWithNoItemsOnBasket_returnsCorrectViewModel() {
        
        let product = TestUtils.someProductA
        let quantity = 0
        
        let viewModel = factory.createProductCellViewModel(for: product, amount: quantity)
        
        XCTAssert(viewModel.nameText == product.name)
        XCTAssert(viewModel.amountText == "")
        XCTAssert(viewModel.priceText == "$5.00")
        XCTAssert(viewModel.hasPromotion == false)
        XCTAssertNotNil(viewModel.image)
    }
    
    func test_createProductCellViewModel_productWithPromotion_returnsCorrectViewModel() {
        
        var product = TestUtils.someProductA
        product.updatePromotion(promotion: TestUtils.somePromotion)
        let quantity = 0
        
        let viewModel = factory.createProductCellViewModel(for: product, amount: quantity)
        
        XCTAssert(viewModel.nameText == product.name)
        XCTAssert(viewModel.amountText == "")
        XCTAssert(viewModel.priceText == "$5.00")
        XCTAssert(viewModel.hasPromotion == true)
        XCTAssertNotNil(viewModel.image)
    }
    
    func test_createProductCellViewModel_voucherProduct_imageCorrect() {
        
        let productVoucher = Product(code: "VOUCHER", name: "", price: 1.0, promotion: nil)
        
        let viewModel = factory.createProductCellViewModel(for: productVoucher, amount: 0)
        
        XCTAssertEqual(viewModel.image, UIImage(named: "voucher"))
    }
    
    func test_createProductCellViewModel_tshirtProduct_imageCorrect() {
        
        let productTshirt = Product(code: "TSHIRT", name: "", price: 1.0, promotion: nil)
        
        let viewModel = factory.createProductCellViewModel(for: productTshirt, amount: 0)
        
        XCTAssertEqual(viewModel.image, UIImage(named: "tshirt"))
    }
    
    func test_createProductCellViewModel_mugProduct_imageCorrect() {
        
        let productMug = Product(code: "MUG", name: "", price: 1.0, promotion: nil)
        
        let viewModel = factory.createProductCellViewModel(for: productMug, amount: 0)
        
        XCTAssertEqual(viewModel.image, UIImage(named: "mug"))
    }
    
    func test_createProductCellViewModel_uknownProduct_imageCorrect() {
        
        let productUnknown = Product(code: "BOOK", name: "", price: 1.0, promotion: nil)
        
        let viewModel = factory.createProductCellViewModel(for: productUnknown, amount: 0)
        
        XCTAssertEqual(viewModel.image, UIImage(named: "unknown"))
    }
}
