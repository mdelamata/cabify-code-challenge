//
//  CheckoutTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import XCTest
@testable import Cabifycc

class CheckoutTests: XCTestCase {
    
    //MARK:- Total and Savings
    
    func test_checkout_productWith2x1AndNormal1_returnsTotalAndSavingsCorrectly() {
        
        var productA = TestUtils.someProductA
        let promo2x1 = Promotion(productCode: productA.code,
                                 description: "",
                                 typeCode: .twoForOne)
        
        productA.updatePromotion(promotion: promo2x1)
        
        let checkoutItem1 = CheckoutItem(product: productA, quantity: 10)
        let checkoutItem2 = CheckoutItem(product: TestUtils.someProductB, quantity: 1)

        let checkout = Checkout(items: [checkoutItem1, checkoutItem2])

        let price1 = checkoutItem1.afterPromotionPrice
        let price2 = checkoutItem2.afterPromotionPrice
            
        XCTAssertEqual(checkout.total, price1 + price2)
        
        let savings1 = checkoutItem1.beforePromotionPrice - checkoutItem1.afterPromotionPrice
        let savings2 = checkoutItem2.beforePromotionPrice - checkoutItem2.afterPromotionPrice
        
        XCTAssertEqual(checkout.savings, savings1 + savings2)
    }
    
    func test_checkout_productsWithoutPromotion_returnsTotalAndSavingsCorrectly() {
        
        let checkoutItem1 =  CheckoutItem(product: TestUtils.someProductA, quantity: 1)
        let checkoutItem2 =  CheckoutItem(product: TestUtils.someProductB, quantity: 10)
        
        let checkout = Checkout(items: [checkoutItem1, checkoutItem2])
        
        let price1 = checkoutItem1.beforePromotionPrice
        let price2 = checkoutItem2.beforePromotionPrice
        
        XCTAssertEqual(checkout.total, price1 + price2)
        XCTAssertEqual(checkout.savings, 0)
    }
    
    //MARK:- Quantity
    
    func test_checkout_returnsProductsQuantityCorrectly() {
        
        let checkoutItem1 =  CheckoutItem(product: TestUtils.someProductA, quantity: 1)
        let checkoutItem2 =  CheckoutItem(product: TestUtils.someProductB, quantity: 10)
        
        let checkout = Checkout(items: [checkoutItem1, checkoutItem2])
        
        let quantity1 = checkoutItem1.quantity
        let quantity2 = checkoutItem2.quantity
        
        XCTAssertEqual(checkout.productsQuantity, quantity1 + quantity2)
    }
    
    //MARK:- Conversion to Basket
    
    func test_checkout_toBasket_makesConversionCorrectly() {
        
        let checkoutItem1 =  CheckoutItem(product: TestUtils.someProductA, quantity: 1)
        let checkoutItem2 =  CheckoutItem(product: TestUtils.someProductB, quantity: 10)
        
        let checkout = Checkout(items: [checkoutItem1, checkoutItem2])
        
        let basket = checkout.toBasket
        
        XCTAssertEqual(basket.keys.count, checkout.items.count)
        
        XCTAssertEqual(basket[TestUtils.someProductA], 1)
        XCTAssertEqual(basket[TestUtils.someProductB], 10)
    }
    
    //MARK:- Empty initializer

    func test_checkout_emptyInitializer_returnsEmptyCheckout() {
        
        XCTAssert(Checkout.empty().items.isEmpty)
    }
}
