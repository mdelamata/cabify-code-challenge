//
//  ListTableViewDataSourceTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import XCTest
import UIKit

class ListTableViewDataSourceTests: XCTestCase {
    
    var mockViewModel: MockListViewModel!
    var listTableViewDataSource: ListTableViewDataSource!
    
    override func setUp() {
        super.setUp()
        
        mockViewModel = MockListViewModel()
        listTableViewDataSource = ListTableViewDataSource(viewModel: mockViewModel)
    }
    
    override func tearDown() {
        mockViewModel = nil
        listTableViewDataSource = nil
        super.tearDown()
    }
    
    //MARK:- UITableViewDataSource
    
    func test_tableViewNumberOfRowsInSection_callsViewModelNumberOfItems() {
        
        _ = listTableViewDataSource.tableView(UITableView(), numberOfRowsInSection: 0)
        XCTAssertTrue(mockViewModel.numberOfItemsWasCalled)
    }
    
    func test_tableViewCellForRowAtIndexPath_callsViewModelGetCellViewModel() {
        
        let expectedIndexPath = IndexPath(row: 0, section: 0)
        mockViewModel.expectedProductCellViewModelToReturn = TestUtils.someProductCellViewModel
        
        let mockProductTableViewCell = MockProductTableViewCell()

        let mockTableView = MockTableView()
        mockTableView.dequeuedCellToReturn = mockProductTableViewCell
        
        _ = listTableViewDataSource.tableView(mockTableView, cellForRowAt: expectedIndexPath)

        let receivedIndexPath = try? XCTUnwrap(mockViewModel.getCellViewModelAtIndexPathWasCalledWith)
        XCTAssertEqual(receivedIndexPath, expectedIndexPath)
        
        XCTAssertTrue(mockProductTableViewCell.configureWasCalled)
    }
}
