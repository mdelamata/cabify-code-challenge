//
//  ListTableViewDelegateTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import XCTest
import UIKit


class ListTableViewDelegateTests: XCTestCase {

    var mockViewModel: MockListViewModel!
    var listTableViewDelegate: ListTableViewDelegate!
    
    override func setUp() {
        super.setUp()
        
        mockViewModel = MockListViewModel()
        listTableViewDelegate = ListTableViewDelegate(viewModel: mockViewModel)
    }
    
    override func tearDown() {
        mockViewModel = nil
        listTableViewDelegate = nil
        super.tearDown()
    }
    
    //MARK:- UITableViewDelegate

    func test_tableViewDidSelectRowAtIndexPath_forwardsCallToViewModel() {
        
        let mockTableView = MockTableView()
        let expectedIndexPath = IndexPath(row: 0, section: 0)
        
        listTableViewDelegate.tableView(mockTableView, didSelectRowAt: expectedIndexPath)
        
        let receivedIndexPath = try? XCTUnwrap(mockTableView.deselectRowWasCalledAtIndexPath)
        XCTAssertEqual(expectedIndexPath, receivedIndexPath)

        let receivedIndexPathFromViewModel = try? XCTUnwrap(mockViewModel.didSelectRowAtIndexPathWasCalledWith)
        XCTAssertEqual(expectedIndexPath, receivedIndexPathFromViewModel)
    }
}
