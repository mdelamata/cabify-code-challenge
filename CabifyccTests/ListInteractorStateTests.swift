//
//  ListInteractorStateTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import XCTest
@testable import Cabifycc

class ListInteractorStateTests: XCTestCase {

    func testMembersEquality() {
        checkEqual(a: .idle, b: .idle)
        checkEqual(a: .failure(NetworkError.noInternetError),
                   b: .failure(NetworkError.noInternetError))
        checkEqual(a: .loading, b: .loading)
        checkEqual(a: .success, b: .success)
    }

    func testMembersNonEquality() {
        checkNotEqual(a: .idle, b: .loading)
        checkNotEqual(a: .failure(NetworkError.noInternetError),
                      b: .failure(NetworkError.jsonDecodingError))
        checkNotEqual(a: .loading, b: .success)
        checkNotEqual(a: .success, b: .idle)
    }
    
    func checkEqual(a: ListInteractorState, b: ListInteractorState) {
        XCTAssertEqual(a, b)
    }

    func checkNotEqual(a: ListInteractorState, b: ListInteractorState) {
        XCTAssertNotEqual(a, b)
    }
}
