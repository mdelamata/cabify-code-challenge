//
//  ProductTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import XCTest
@testable import Cabifycc

class ProductTests: XCTestCase {
    
    func test_product_equality() {
        
        let productA = Product(code: "CODE", name: "Name1", price: 10)
        let productB = Product(code: "CODE", name: "Name1", price: 10)
        
        XCTAssertEqual(productA, productB)
    }
    
    func test_product_nonEquality() {
        
        let productA = Product(code: "CODE", name: "Name1", price: 10)
        let productB = Product(code: "CODE_", name: "Name1", price: 10)
        
        XCTAssertNotEqual(productA, productB)
    }
    
}
