//
//  CoordinatorFactoryTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import XCTest
@testable import Cabifycc

class CoordinatorFactoryTests: XCTestCase {

    var coordinatorFactory: CoordinatorFactory! = CoordinatorFactory()
    var navigationController: UINavigationController! = UINavigationController()
    
    override func tearDown() {
        coordinatorFactory = nil
        navigationController = nil
        super.tearDown()
    }

    func test_createListCoordinator_returnsListCoordinator() {
        
        let coordinatorUT = coordinatorFactory.createListCoordinator(navigationController: navigationController)
        
        XCTAssertTrue(coordinatorUT is ListCoordinating)
        XCTAssertEqual(coordinatorUT.navigationController, navigationController)
    }
    
    func test_createBasketCoordinator_returnsListCoordinator() {
        
        let coordinatorUT = coordinatorFactory.createBasketCoordinator(navigationController: navigationController,
                                                                       with: Checkout.empty())
        
        XCTAssertTrue(coordinatorUT is BasketCoordinating)
        XCTAssertEqual(coordinatorUT.navigationController, navigationController)
    }
}
