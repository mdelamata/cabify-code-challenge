//
//  BasketTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 27/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

import XCTest
@testable import Cabifycc

class BasketTests: XCTestCase {

    func test_basket_toCheckout_makesConversionCorrectly() {
        
        var basket = Basket()
        basket[TestUtils.someProductA] = 1
        basket[TestUtils.someProductB] = 10
        
        let checkout = basket.toCheckout
        
        
        XCTAssertEqual(checkout.productsQuantity, 11)
        XCTAssertEqual(checkout.items.count, 2)
        
        let checkoutItemForProductA = checkout.items
            .filter({ $0.product == TestUtils.someProductA})
            .first
        
        XCTAssertEqual(checkoutItemForProductA?.product, TestUtils.someProductA)
        XCTAssertEqual(checkoutItemForProductA?.quantity, 1)
    }
    
}
