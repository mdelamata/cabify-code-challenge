//
//  ProductTableViewCellTests.swift
//  CabifyccTests
//
//  Created by Manuel de la Mata Sáez on 28/01/2020.
//  Copyright © 2020 Manuel de la Mata. All rights reserved.
//

@testable import Cabifycc
import XCTest
import UIKit

class ProductTableViewCellTests: XCTestCase {
    

    func test_reusableIdentifier_isCorrect() {
        XCTAssertEqual(ProductTableViewCell.reusableIdentifier, "productCell")
    }
    
    func test_configure_configuredCorrectly() {
        
        let cell = productCell()
        
        let viewModel = TestUtils.someProductCellViewModel
        
        cell.configure(viewModel: viewModel)
        
        XCTAssertEqual(cell.productImageView.image, viewModel.image)
        XCTAssertEqual(cell.nameLabel.text, viewModel.nameText)
        XCTAssertEqual(cell.amountLabel.text, viewModel.amountText)
        XCTAssertEqual(cell.amountLabel.text, viewModel.amountText)
    }
    
    func test_configure_noPromotion_hidesPromotionBadge() {
        
        let cell = productCell()
        let viewModel = TestUtils.someProductCellViewModel
        
        cell.configure(viewModel: viewModel)

        XCTAssertTrue(cell.promotionBadgeImageView.isHidden)
        
    }
    
    func test_configure_withPromotion_showsPromotionBadge() {
         
         let cell = productCell()
         let viewModel = TestUtils.someProductCellViewModelWithPromotion
         
         cell.configure(viewModel: viewModel)

         XCTAssertFalse(cell.promotionBadgeImageView.isHidden)
     }
    
    
    func productCell() -> ProductTableViewCell {
        
        let navigationController = UINavigationController()
        let listInteractor = ListInteractor()
        let listCoordinator = ListCoordinator(navigationController: navigationController)
        let viewModel = ListViewModel(coordinator: listCoordinator,
                                      interactor: listInteractor)
        
        let listViewController = UIStoryboard.main.instantiateViewController(identifier: "ListViewController") as! ListViewController
        listViewController.viewModel = viewModel
        
        listInteractor.products.send([TestUtils.someProductA])
        
        _ = listViewController.view
        
        return viewModel.listTableViewDataSource.tableView(listViewController.tableView, cellForRowAt: IndexPath(row: 0, section: 0)) as! ProductTableViewCell
    }
}
